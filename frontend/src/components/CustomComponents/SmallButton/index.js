import React from 'react';
import './style.scss';

const SmallButton = (props) => {
  return (
    <button
      onClick={props.onClick}
      className={"small-button " + props.className}
      type={props.type}
    >
      <span>{props.text}</span>
    </button>
  )
};

export default SmallButton