<?php

namespace App\Cart;

use App\Entity\AttributeValue;

class CartAttributesConverter
{
    /**
     * @param array|AttributeValue[] $attributeValues
     *
     * @return array
     */
    public function arrayValuesToArray(array $attributeValues): array
    {
        $list = [];

        /** @var AttributeValue $attributeValue */
        foreach ($attributeValues as $attributeValue) {
            $list[] = [
                'attributeId' => $attributeValue->getAttribute()->getId(),
                'attributeName' => $attributeValue->getAttribute()->getName(),
                'attributeValue' => $attributeValue->getValue(),
                'attributeValueId' => $attributeValue->getId(),
            ];
        }

        return array_values($list);
    }

    /**
     * @param array $attributes
     *
     * @return string
     */
    public function arraySortToString(array $attributes): string
    {
        $sorted = $this->sortArray($attributes);

        return json_encode($sorted);
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    private function sortArray(array $attributes): array
    {
        $list = [];

        foreach ($attributes as $attribute) {
            $index = $attribute['attributeId'] . ':' . $attribute['attributeValueId'];
            $list[$index] = $attribute;
        }

        ksort($list);

        return $list;
    }
}
