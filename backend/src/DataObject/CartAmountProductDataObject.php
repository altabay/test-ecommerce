<?php

namespace App\DataObject;

use App\Entity\Cart;
use Symfony\Component\Validator\Constraints as Assert;

class CartAmountProductDataObject extends CartProductDataObject
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(value=1)
     */
    private $amount;

    public function __construct()
    {
        parent::__construct();
        $this->amount = 0;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAmount(int $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
