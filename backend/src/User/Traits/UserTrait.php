<?php

namespace App\User\Traits;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait UserTrait
{
    /**
     * @param TokenStorageInterface $tokenStorage
     *
     * @return User|null
     */
    private function getUser(TokenStorageInterface $tokenStorage): ?User
    {
        if (null === $token = $tokenStorage->getToken()) {
            return null;
        }

        if (!\is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }
}
