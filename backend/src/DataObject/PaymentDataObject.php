<?php

namespace App\DataObject;

class PaymentDataObject
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $responseData;

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setStatus(string $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseData(): ?string
    {
        return $this->responseData;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setResponseData(string $value): self
    {
        $this->responseData = $value;

        return $this;
    }
}
