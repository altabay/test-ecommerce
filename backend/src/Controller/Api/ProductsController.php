<?php

namespace App\Controller\Api;

use App\Controller\Traits\CreateNamelessFormTrait;
use App\Entity\Product;
use App\Filter\ProductListFilter;
use App\Form\ProductListFilterType;
use App\Repository\ProductRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/products"
 * )
 */
class ProductsController extends BaseFOSRestController
{
    use CreateNamelessFormTrait;

    /**
     * @Rest\Route(
     *     path="/filter",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $productsListFilter = new ProductListFilter();
        $form = $this->createNamelessForm(ProductListFilterType::class, $productsListFilter);
        $form->handleRequest($request);

        /** @var ProductRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Product::class);

        return $this->view(
            [
                'totalRecords' => $repository->getFilteredTotal($productsListFilter),
                'list' => $this
                    ->get('app.pack.product_pack')
                    ->packList(
                        $repository->getFilteredList($productsListFilter)
                    ),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/get/{id}",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function getAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($request->get('id'))
        ;

        if (!$product) {
            return $this->errorView(Response::HTTP_NOT_FOUND, 'Product not found.');
        }

        return $this->view(
            $this->get('app.pack.product_pack')->pack($product),
            Response::HTTP_OK
        );
    }
}
