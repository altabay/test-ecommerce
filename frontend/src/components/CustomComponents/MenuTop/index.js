import React from 'react';
import './style.scss';
import { Navbar, Nav, Image } from 'react-bootstrap';
import ModalLoginAndSignUp from './ModalLoginAndSignUp';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout, changeStep } from 'actions';

const mapStateToProps = ({ userInfo, cardsInfo }) => {
  return {
    token: userInfo.token,
    email: userInfo.email,
    page: cardsInfo.page,
  }
};

class MenuTop extends React.Component {
  state = {
    show: false
  };

  toggleModal = () => {
    const { show } = this.state;
    this.setState({
      show: !show
    })
  };

  logout = () => {
    const { dispatch } = this.props;
    dispatch(logout());
    dispatch(changeStep(0));
  };

  render() {
    const { show } = this.state,
      { token, email, page } = this.props;

    return (
      <Navbar bg="dark" variant="dark" className="justify-content-between menu-top">
        <Navbar.Brand className="menu-top-logo">
          <Link to={"/page/" + page}>
            <Image src="/images/logo.png" alt="logo"/>
          </Link>
        </Navbar.Brand>
        {token ? (
          <div className="d-inline-block d-md-flex">
            <Nav.Item className="menu-top-email">{email}</Nav.Item>
            <Nav.Item
              className="menu-top-auth"
              onClick={this.logout}
            >
              Logout
            </Nav.Item>
          </div>
        ) : (
          <Nav.Item
            className="menu-top-auth"
            onClick={this.toggleModal}
          >
            Sign in | Login
          </Nav.Item>
        )}
        <ModalLoginAndSignUp
          show={show}
          toggleModal={this.toggleModal}
        />
      </Navbar>
    )
  }
}

export default connect(mapStateToProps)(MenuTop)