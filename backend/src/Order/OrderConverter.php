<?php

namespace App\Order;

use App\DataObject\OrderDataObject;
use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\User;

class OrderConverter
{
    /**
     * @var OrderProductConverter
     */
    private $orderProductConverter;

    /**
     * @param OrderProductConverter $orderProductConverter
     */
    public function __construct(
        OrderProductConverter $orderProductConverter
    ) {
        $this->orderProductConverter = $orderProductConverter;
    }

    /**
     * @param OrderDataObject $orderDataObject
     * @param User $user
     * @param Cart[] $carts
     *
     * @return Order
     */
    public function dataToOrder(OrderDataObject $orderDataObject, User $user, $carts): Order
    {
        $order = (new Order())
            ->setUser($user)
            ->setUserEmail($user->getEmail())
            ->setFirstName($orderDataObject->getFirstName())
            ->setLastName($orderDataObject->getLastName())
            ->setCountry($orderDataObject->getCountry())
            ->setState($orderDataObject->getState())
            ->setZip($orderDataObject->getZip())
            ->setCity($orderDataObject->getCity())
            ->setAddress($orderDataObject->getAddress())
            ->setShipping($orderDataObject->getShipping())
            ->setShippingDescription(sprintf('%s > %s',
                $orderDataObject->getShipping()->getShippingRegion()->getName(),
                $orderDataObject->getShipping()->getShippingType()
            ))
        ;

        foreach ($carts as $cart) {
            $orderProduct = $this->orderProductConverter->cartToOrderProduct($cart);
            $orderProduct->setOrder($order);
            $order
                ->addProduct($orderProduct)
                ->setProductsPrice($order->getProductsPrice() + $orderProduct->getProductPrice() * $orderProduct->getAmount())
            ;
        }

        $order->setTotalPrice($order->getProductsPrice() + $orderDataObject->getShipping()->getShippingCost());

        return $order;
    }
}
