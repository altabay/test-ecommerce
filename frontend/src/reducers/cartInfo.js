const initialState = {
  cartId: localStorage.getItem('cartId') ? localStorage.getItem('cartId') : null,
  cartData: [],
  totalPrice: null
};

const cartInfo = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_CART_ID':
      const cartId = action.id ? action.id : '';
      localStorage.setItem('cartId', cartId);
      return { ...state, cartId: action.id };
    case 'CHANGE_CART_DATA':
      return { ...state, cartData: action.data };
    case 'CHANGE_CART_TOTAL_PRICE':
      return { ...state, totalPrice: action.price };
    default:
      return state
  }
};

export default cartInfo