<?php

namespace App\Pack;

use App\Entity\Department;

class DepartmentPack
{
    /**
     * @var CategoryPack
     */
    private $categoryPack;

    /**
     * @param CategoryPack $categoryPack
     */
    public function __construct(CategoryPack $categoryPack)
    {
        $this->categoryPack = $categoryPack;
    }

    /**
     * @param Department $department
     *
     * @return array
     */
    public function pack(Department $department): array
    {
        return [
            'id' => $department->getId(),
            'name' => (string)$department->getName(),
            'description' => (string)$department->getDescription(),
            'categories' => $this->categoryPack->packList($department->getCategories()->toArray()),
        ];
    }

    /**
     * @param array|Department[] $departments
     *
     * @return array
     */
    public function packList(array $departments): array
    {
        $list = [];

        foreach ($departments as $department) {
            $list[] = $this->pack($department);
        }

        return $list;
    }
}
