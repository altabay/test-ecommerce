<?php

namespace App\Controller\Api;

use App\Controller\Traits\CreateNamelessFormTrait;
use App\DataObject\UserDataObject;
use App\Entity\User;
use App\Form\UserType;
use App\User\Manager\OAuthManager;
use App\User\Manager\UserManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/user"
 * )
 */
class UserController extends BaseFOSRestController
{
    use CreateNamelessFormTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var OAuthManager
     */
    private $oauthManager;

    /**
     * @param UserManager $userManager
     * @param OAuthManager $oauthManager
     */
    public function __construct(
        UserManager $userManager,
        OAuthManager $oauthManager
    ) {
        $this->userManager = $userManager;
        $this->oauthManager = $oauthManager;
    }

    /**
     * @Rest\Route(
     *     path="/register",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function registerAction(ParamFetcherInterface $paramFetcher, Request $request): View
    {
        $userDataObject = new UserDataObject();
        $form = $this->createNamelessForm(UserType::class, $userDataObject);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        /** @var User $user */
        $user = $this->userManager->register($userDataObject->getEmail(), $userDataObject->getPassword());

        return $this->view(
            $this->serveTokenResponse($user),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/login",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function loginAction(ParamFetcherInterface $paramFetcher, Request $request): View
    {
        $userDataObject = new UserDataObject();
        $form = $this->createNamelessForm(UserType::class, $userDataObject);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        /** @var User $user */
        $user = $this->userManager->login($userDataObject->getEmail(), $userDataObject->getPassword());

        return $this->view(
            $this->serveTokenResponse($user),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/google/{token}",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function googleAction(ParamFetcherInterface $paramFetcher, Request $request): View
    {
        try {
            $data = $this
                ->get('app.social.provider.google_provider')
                ->getCallData($request->get('token'))
            ;
        } catch (\Exception $e) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }

        if (!isset($data['email'])) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Email is not provided by Google.');
        }

        /** @var User $user */
        $user = $this->userManager->google($data['email']);

        return $this->view(
            $this->serveTokenResponse($user),
            Response::HTTP_OK
        );
    }

    /**
     * @param User $user
     *
     * @return array
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function serveTokenResponse(User $user): array
    {
        $token = $this->oauthManager->getCreateToken($user);

        return [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'token' => $token->getToken(),
        ];
    }
}
