import React from 'react';
import { Col, Form } from 'react-bootstrap';

class UserInfo extends React.Component {

  render() {
    const { changeDeliveryData, deliveryData } = this.props,
      data = [
        {
          key: 'firstName',
          text: 'First name'
        }, {
          key: 'lastName',
          text: 'Last name'
        }, {
          key: 'address',
          text: 'Address'
        }, {
          key: 'city',
          text: 'City'
        }, {
          key: 'state',
          text: 'State'
        }, {
          key: 'country',
          text: 'Country'
        }, {
          key: 'zip',
          text: 'Zip Code'
        }
      ];

    return (
      <Form.Row className="checkout-items-delivery-row">
        {data.map(item =>
          <Form.Group as={Col} xs={12} md={6} xl={4} key={item.key}>
            <Form.Label>{item.text}</Form.Label>
            <Form.Control
              value={deliveryData[item.key]}
              required
              type="text"
              onChange={e => changeDeliveryData(item.key, e.target.value)}
            />
            <Form.Control.Feedback
              type="invalid"
            >
              Please provide a valid {item.key}.
            </Form.Control.Feedback>
          </Form.Group>
        )}
      </Form.Row>
    )
  }
}

export default UserInfo