import React from 'react';
import Helmet from 'react-helmet';
import NotFound from './NotFound';

class NotFoundPage extends React.Component {

  render() {

    return (
      <div>
        <Helmet title="404"/>
        <NotFound/>
      </div>
    )
  }
}

export default NotFoundPage