<?php

namespace App\Pack;

use App\Entity\Category;

class CategoryPack
{
    /**
     * @param Category $category
     *
     * @return array
     */
    public function pack(Category $category): array
    {
        return [
            'id' => $category->getId(),
            'name' => (string)$category->getName(),
            'description' => (string)$category->getDescription(),
        ];
    }

    /**
     * @param array|Category[] $categories
     *
     * @return array
     */
    public function packList(array $categories): array
    {
        $list = [];

        foreach ($categories as $category) {
            $list[] = $this->pack($category);
        }

        return $list;
    }
}
