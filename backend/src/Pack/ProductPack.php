<?php

namespace App\Pack;

use App\Entity\Product;
use App\Formatter\PriceFormatter;
use Symfony\Component\HttpFoundation\RequestStack;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ProductPack
{
    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @var AttributePack
     */
    private $attributePack;

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param PriceFormatter $priceFormatter
     * @param AttributePack $attributePack
     * @param UploaderHelper $uploaderHelper
     * @param RequestStack $requestStack
     */
    public function __construct(
        PriceFormatter $priceFormatter,
        AttributePack $attributePack,
        UploaderHelper $uploaderHelper,
        RequestStack $requestStack
    ) {
        $this->priceFormatter = $priceFormatter;
        $this->attributePack = $attributePack;
        $this->uploaderHelper = $uploaderHelper;
        $this->requestStack = $requestStack;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public function pack(Product $product): array
    {
        $url = $this->requestStack->getMasterRequest()->getSchemeAndHttpHost();

        return [
            'id' => $product->getId(),
            'name' => (string)$product->getName(),
            'description' => (string)$product->getDescription(),
            'image' => $product->getImage()
                ? $url . $this->uploaderHelper->asset($product, 'imageFile')
                : null,
            'image2' => $product->getImage2()
                ? $url .$this->uploaderHelper->asset($product, 'image2File')
                : null,
            'thumbnail' => $product->getThumbnail()
                ? $url . $this->uploaderHelper->asset($product, 'thumbnailFile')
                : null,
            'price' => (float)$product->getPrice(),
            'priceFormatted' => $this->priceFormatter->format($product->getPrice()),
            'discountedPrice' => $product->getDiscountedPrice()
                ? (float)$product->getDiscountedPrice()
                : null,
            'discountedPriceFormatted' => $product->getDiscountedPrice()
                ? $this->priceFormatter->format($product->getDiscountedPrice())
                : null,
            'attributes' => $this->attributePack->packList($product->getAttributes()),
        ];
    }

    /**
     * @param array|Product[] $categories
     *
     * @return array
     */
    public function packList(array $categories): array
    {
        $list = [];

        foreach ($categories as $product) {
            $list[] = $this->pack($product);
        }

        return $list;
    }
}
