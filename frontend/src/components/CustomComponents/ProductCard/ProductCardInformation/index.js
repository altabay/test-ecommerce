import React from 'react';
import './style.scss';
import ProductCardForm from './ProductCardForm';
import { Link } from "react-router-dom";

const ProductCardInformation = ({ product }) => {

  return (
    <div className="product-card-info">
      <div className="product-card-info-title">
        <Link to={"/product/" + product.id}>
          <p>{product.name}</p>
          <div className="product-card-info-price">
            {product.discountedPrice
              ? <span>
             <span>
              {product.discountedPriceFormatted}
            </span>
            <div className="product-card-price-old">
              {product.priceFormatted}
              </div>
            </span>
              : <span>
              {product.priceFormatted}
            </span>
            }
          </div>
        </Link>
      </div>
      <ProductCardForm
        id={product.id}
        attributes={product.attributes}
      />
    </div>
  )
};

export default ProductCardInformation
