import React from 'react';
import { Row, Col } from 'react-bootstrap';
import ProductCard from 'components/CustomComponents/ProductCard';
import { connect } from 'react-redux';

const mapStateToProps = ({ menuInfo, cardsInfo }) => {
  return {
    data: cardsInfo.data,
  }
};

class Products extends React.Component {

  render() {
    const { data } = this.props;
    return (
      <Row>
        {data && data.map(product => {
            return (
              <Col xl={4} md={6} sm={12} key={product.id}>
                <ProductCard
                  product={product}
                />
              </Col>
            )
          }
        )}
      </Row>
    )
  }
}

export default connect(mapStateToProps)(Products)