<?php

namespace App\User\Manager;

use App\Entity\OAuth\AccessToken;
use App\Entity\OAuth\Client;
use App\Entity\User;
use App\Repository\OAuth\AccessTokenRepository;
use App\Repository\OAuth\ClientRepository;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\TokenInterface;

class OAuthManager
{
    private const DEFAULT_SCOPE = 'advertise';
    private const DEFAULT_TOKEN_TTL = 86400;

    /**
     * @var AccessTokenManagerInterface
     */
    private $accessTokenManager;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var AccessTokenRepository
     */
    private $accessTokenRepository;

    /**
     * @var array
     */
    private $oauthServerOptions;

    /**
     * @param AccessTokenManagerInterface $accessTokenManager
     * @param ClientRepository $clientRepository
     * @param AccessTokenRepository $accessTokenRepository
     * @param array $oauthServerOptions
     */
    public function __construct(
        AccessTokenManagerInterface $accessTokenManager,
        ClientRepository $clientRepository,
        AccessTokenRepository $accessTokenRepository,
        array $oauthServerOptions
    ) {
        $this->accessTokenManager = $accessTokenManager;
        $this->clientRepository = $clientRepository;
        $this->accessTokenRepository = $accessTokenRepository;
        $this->oauthServerOptions = $oauthServerOptions;
    }

    /**
     * @param User $user
     *
     * @return TokenInterface
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getCreateToken(User $user): TokenInterface
    {
        $client = $this->clientRepository->findFirst();

        if (!$client) {
            throw new \LogicException('OAuth client was not created');
        }

        if ($token = $this->getRefreshToken($client, $user)) {
            return $token;
        }

        return $this->createToken($client, $user);
    }

    /**
     * @return int
     */
    private function getTokenTtl(): int
    {
        return $this->oauthServerOptions['access_token_lifetime'] ?? self::DEFAULT_TOKEN_TTL;
    }

    /**
     * @param Client $client
     * @param User $user
     *
     * @return AccessToken|null
     *
     * @throws \Doctrine\ORM\ORMException
     */
    private function getRefreshToken(Client $client, User $user): ?AccessToken
    {
        $token = $this->accessTokenRepository->findOneBy([
            'client' => $client->getId(),
            'user' => $user->getId(),
        ]);

        if (!$token) {
            return null;
        }

        $token->setExpiresAt(time() + $this->getTokenTtl());
        $this->accessTokenManager->updateToken($token);

        return $token;
    }

    /**
     * @param Client $client
     * @param User $user
     *
     * @return TokenInterface
     *
     * @throws \Exception
     */
    private function createToken(Client $client, User $user): TokenInterface
    {
        $token = $this->accessTokenManager->createToken();
        $token->setToken(md5($user->getPassword() . '/' . random_int(1, 931415926536)));
        $token->setClient($client);
        $token->setExpiresAt(time() + $this->getTokenTtl());
        $token->setScope(self::DEFAULT_SCOPE);
        $token->setUser($user);
        $this->accessTokenManager->updateToken($token);

        return $token;
    }
}
