<?php

namespace App\Social\Provider;

class GoogleProvider
{
    private const API_URL = 'https://www.googleapis.com/oauth2/v1';

    /**
     * @param string $token
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCallData(string $token): array
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', self::API_URL . '/userinfo?alt=json', [
            'headers' => [
                'Authorization' => sprintf('Bearer %s', $token),
            ],
        ]);
        $body = json_decode($response->getBody()->getContents(), true);

        if (empty($body['id'])) {
            throw new \LogicException('No user ID specified');
        }

        return [
            'id' => $body['id'],
            'email' => $body['email'] ?? null,
        ];
    }
}
