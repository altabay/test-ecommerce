<?php

namespace App\Filter;

use App\Entity\Category;
use App\Entity\Department;
use Symfony\Component\Validator\Constraints as Assert;

class ProductListFilter
{
    private const DEFAULT_PAGE = 1;
    private const DEFAULT_PER_PAGE = 12;

    /**
     * @var string|null
     */
    private $search;

    /**
     * @var Department|null
     */
    private $department;

    /**
     * @var Category|null
     */
    private $category;

    /**
     * @var int
     *
     * @Assert\GreaterThanOrEqual(
     *     value=1
     * )
     */
    private $page;

    /**
     * @var int
     *
     * @Assert\GreaterThanOrEqual(
     *     value=1
     * )
     */
    private $perPage;

    public function __construct()
    {
        $this->page = self::DEFAULT_PAGE;
        $this->perPage = self::DEFAULT_PER_PAGE;
    }

    /**
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return $this->search;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setSearch(?string $value): self
    {
        $this->search = $value;

        return $this;
    }

    /**
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    /**
     * @param Department|null $value
     *
     * @return $this
     */
    public function setDepartment(?Department $value): self
    {
        $this->department = $value;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $value
     *
     * @return $this
     */
    public function setCategory(?Category $value): self
    {
        $this->category = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setPage(int $value): self
    {
        $this->page = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setPerPage(int $value): self
    {
        $this->perPage = $value;

        return $this;
    }
}
