const initialState = {
  token: localStorage.getItem('token') ? localStorage.getItem('token') : null,
  email: localStorage.getItem('email') ? localStorage.getItem('email') : null
};

const userInfo = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER_OR_LOGIN_USER':
      localStorage.setItem('token', action.params.token);
      localStorage.setItem('email', action.params.email);
      return { ...action.params };
    case 'LOGOUT':
      localStorage.setItem('token', '');
      localStorage.setItem('email', '');
      return { token: null, email: null };
    default:
      return state
  }
};

export default userInfo