import React from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import LoginTab from 'components/CustomComponents/LoginTab';
import SignUpTab from 'components/CustomComponents/SignUpTab';

class LoginOrSignUp extends React.Component {
  state = {
    key: 'login'
  };

  changeTab = (key) => {
    this.setState({ key })
  };

  render() {
    const { key } = this.state;
    const { nextStep } = this.props;

    return (
      <div className="checkout-items-authorization">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={key => this.changeTab(key)}
        >
          <Tab eventKey="login" title="Login">
            <LoginTab
              success={nextStep}
            />
          </Tab>
          <Tab eventKey="signUp" title="Sign Up">
            <SignUpTab
              success={nextStep}
            />
          </Tab>
        </Tabs>
      </div>
    )
  }
}

export default LoginOrSignUp