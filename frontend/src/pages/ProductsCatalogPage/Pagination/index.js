import React from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Pagination = ({ pageNumbersArray, changePageNumber }) => {
  return (
    <Row className="pager">
      <ul className="pager-pagination">
        {pageNumbersArray.map(item => {
            return (
              <Link
                onClick={() => changePageNumber(item.pageNo)}
                to={"/page/" + item.pageNo}
                key={item.key}
                className={item.classNameLink}
              >
                <li className={"pager-pagination-item " + item.className}>

                  <span>{item.view}</span>
                </li>
              </Link>
            )
          }
        )}
      </ul>
    </Row>
  )
};


export default Pagination