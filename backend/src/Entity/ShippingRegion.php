<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShippingRegionRepository")
 */
class ShippingRegion
{
    use IdTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var Shipping[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Shipping",
     *     mappedBy="shippingRegion",
     *     cascade={"persist","remove"}
     * )
     */
    private $shippings;

    public function __construct()
    {
        $this->shippings = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return Shipping[]|ArrayCollection|Collection
     */
    public function getShippings()
    {
        return $this->shippings;
    }

    /**
     * @param Shipping $value
     *
     * @return $this
     */
    public function addShipping($value)
    {
        if (!$this->shippings->contains($value)) {
            $this->shippings->add($value);
        }

        return $this;
    }

    /**
     * @param Shipping $value
     *
     * @return $this
     */
    public function removeShipping($value)
    {
        if ($this->shippings->contains($value)) {
            $this->shippings->removeElement($value);
        }

        return $this;
    }
}
