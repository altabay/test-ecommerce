import React from 'react';
import { Carousel } from "antd";

const ImageDetails = ({ images, imgActiveStatus, setActiveImg, slider }) => {
  return (
    <div>
      <div className="product-details-item">
        <div className="product-details-item-img">
          <Carousel
            ref={node => (slider = node)}
            autoplay={false}
            dots={false}
            effect="fade"
          >
            {images.map((image, index) => (
              <div key={index}>
                <img className="product-details-item-img-item" src={image} alt="" />
              </div>
            ))}
          </Carousel>
        </div>
      </div>
      <div className="product-details-photos clearfix">
        {images.map((image, index) => (
          <div
            key={index}
            onClick={e =>
              slider.slick.innerSlider.slickGoTo(index) & setActiveImg(index)
            }
            className={
              imgActiveStatus[index] === 'active'
                ? 'product-details-photos-item product-details-photos-item--active'
                : 'product-details-photos-item'
            }
          >
            <img src={image} alt=""/>
          </div>
        ))}
      </div>
    </div>
  )
};

export default ImageDetails