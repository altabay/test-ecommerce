const initialState = {
  departments : [],
  categories: [],
  filterCategories: [],
  category: null,
  department: null,
  search: null
};

const menuInfo = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_MENU_INFO':
      return {...state, ...action.params};
    case 'FILTER_CATEGORIES_INFO':
      const newState = {...state},
        {categories} = newState,
        filterCategories = categories.filter(item => item.departmentId === action.id);
      return {...state, filterCategories};
    case 'UPDATE_MENU_PARAMS':
      return {...state, ...action.param};
    default:
      return state
  }
};

export default menuInfo