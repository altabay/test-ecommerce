import React from 'react';
import './style.scss';

const Select = ({ options, changeAttributesValue, name, value }) => {
  return (
    <select
      value={value}
      className="select"
      onChange={(e) => changeAttributesValue(e.target.value, name)}
    >
      {options.map(item =>
        <option
          value={item.id}
          key={item.id}
        >
          {item.name}
        </option>
      )}
    </select>
  )
};

export default Select