<?php

namespace App\Repository;

use App\Entity\Product;
use App\Filter\ProductListFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ProductRepository extends EntityRepository
{
    /**
     * @param ProductListFilter $productListFilter
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFilteredTotal(ProductListFilter $productListFilter): int
    {
        $qb = $this->createQueryBuilderByFilter($productListFilter);
        $qb->select('count(distinct p.id)');

        return (int)$qb->getQuery()->getSingleScalarResult();
    }
    /**
     * @param ProductListFilter $productListFilter
     *
     * @return array|Product[]
     */
    public function getFilteredList(ProductListFilter $productListFilter)
    {
        $qb = $this->createQueryBuilderByFilter($productListFilter);
        $qb
            ->setFirstResult(($productListFilter->getPage()-1)*$productListFilter->getPerPage())
            ->setMaxResults($productListFilter->getPerPage())
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param ProductListFilter $productListFilter
     *
     * @return QueryBuilder
     */
    private function createQueryBuilderByFilter(ProductListFilter $productListFilter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('p');
        $qb->leftJoin('p.categories', 'c');

        if ($productListFilter->getSearch()) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('p.name', $qb->expr()->literal('%' . $productListFilter->getSearch() . '%')),
                    $qb->expr()->like('p.description', $qb->expr()->literal('%' . $productListFilter->getSearch() . '%'))
                )
            );
        }

        if ($productListFilter->getDepartment()) {
            $qb
                ->andWhere('c.department = :department')
                ->setParameter(':department', $productListFilter->getDepartment())
            ;
        }

        if ($productListFilter->getCategory()) {
            $qb
                ->andWhere('c.id = :category')
                ->setParameter(':category', $productListFilter->getCategory())
            ;
        }

        return $qb;
    }
}
