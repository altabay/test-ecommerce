<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 */
class Department
{
    use IdTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var Category[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Category",
     *     mappedBy="department",
     *     cascade={"persist","remove"},
     *     orphanRemoval=true
     * )
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->name;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setDescription(?string $value): self
    {
        $this->description = $value;

        return $this;
    }

    /**
     * @return Category[]|ArrayCollection|Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category $value
     *
     * @return $this
     */
    public function addCategory($value)
    {
        if (!$this->categories->contains($value)) {
            $this->categories->add($value);
        }

        return $this;
    }

    /**
     * @param Category $value
     *
     * @return $this
     */
    public function removeCategory($value)
    {
        if ($this->categories->contains($value)) {
            $this->categories->removeElement($value);
        }

        return $this;
    }
}
