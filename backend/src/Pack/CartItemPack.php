<?php

namespace App\Pack;

use App\Entity\Cart;
use App\Formatter\DateTimeFormatter;
use App\Formatter\PriceFormatter;

class CartItemPack
{
    /**
     * @var ProductPack
     */
    private $productPack;

    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @var DateTimeFormatter
     */
    private $dateTimeFormatter;

    /**
     * @param ProductPack $productPack
     * @param PriceFormatter $priceFormatter
     * @param DateTimeFormatter $dateTimeFormatter
     */
    public function __construct(
        ProductPack $productPack,
        PriceFormatter $priceFormatter,
        DateTimeFormatter $dateTimeFormatter
    ) {
        $this->productPack = $productPack;
        $this->priceFormatter = $priceFormatter;
        $this->dateTimeFormatter = $dateTimeFormatter;
    }

    /**
     * @param Cart $cart
     *
     * @return array
     */
    public function pack(Cart $cart): array
    {
        $price = $cart->getProduct()->getFinalPrice();
        $total = $price * $cart->getAmount();

        return [
            'cartProductId' => $cart->getId(),
            'product' => $this->productPack->pack($cart->getProduct()),
            'amount' => $cart->getAmount(),
            'price' => $price,
            'priceFormatted' => $this->priceFormatter->format($price),
            'total' => $total,
            'totalFormatted' => $this->priceFormatter->format($total),
            'added' => $this->dateTimeFormatter->format($cart->getCreatedAt()),
            'attributes' => $cart->getAttributes(),
        ];
    }

    /**
     * @param array|Cart[] $carts
     *
     * @return array
     */
    public function packList(array $carts): array
    {
        $list = [];

        foreach ($carts as $cart) {
            $list[] = $this->pack($cart);
        }

        return $list;
    }
}
