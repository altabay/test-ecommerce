<?php

namespace App\Controller\Traits;

trait CreateNamelessFormTrait
{
    /**
     * @param null $type
     * @param null $data
     * @param array $options
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createNamelessForm($type = null, $data = null, array $options = [])
    {
        return $this->container->get('form.factory')->createNamed(null, $type, $data, $options);
    }
}
