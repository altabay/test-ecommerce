<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190314101757 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE order_product DROP FOREIGN KEY FK_2530ADE68D9F6D38');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, shipping_id INT DEFAULT NULL, user_email VARCHAR(180) NOT NULL, first_name VARCHAR(180) NOT NULL, last_name VARCHAR(180) NOT NULL, country VARCHAR(180) NOT NULL, state VARCHAR(180) NOT NULL, zip VARCHAR(10) NOT NULL, city VARCHAR(180) NOT NULL, address VARCHAR(180) NOT NULL, products_price DOUBLE PRECISION NOT NULL, total_price DOUBLE PRECISION NOT NULL, shipping_description VARCHAR(180) NOT NULL, status VARCHAR(20) NOT NULL, created_at DATETIME DEFAULT \'1980-01-01 00:00:00\' NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_E52FFDEEA76ED395 (user_id), INDEX IDX_E52FFDEE4887F3F8 (shipping_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE4887F3F8 FOREIGN KEY (shipping_id) REFERENCES shipping (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT FK_2530ADE68D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE order_product DROP FOREIGN KEY FK_2530ADE68D9F6D38');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, shipping_id INT DEFAULT NULL, user_email VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, first_name VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, last_name VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, country VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, state VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, zip VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci, city VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, address VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, products_price DOUBLE PRECISION NOT NULL, total_price DOUBLE PRECISION NOT NULL, shipping_description VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, status VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, created_at DATETIME DEFAULT \'1980-01-01 00:00:00\' NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F52993984887F3F8 (shipping_id), INDEX IDX_F5299398A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993984887F3F8 FOREIGN KEY (shipping_id) REFERENCES shipping (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE orders');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT FK_2530ADE68D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
    }
}
