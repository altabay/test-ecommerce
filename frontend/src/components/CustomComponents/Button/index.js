import React from 'react';
import './style.scss';

const Button = (props) => {
  return (
    <button
      className="product-button"
      type={props.type}
    >
      <span>{props.text}</span>
    </button>
  )
};

export default Button