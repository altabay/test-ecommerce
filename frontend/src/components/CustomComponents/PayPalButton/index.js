import React from 'react';
import PaypalExpressBtn from 'react-paypal-express-checkout';
import { connect } from 'react-redux';
import axios from 'axios';

const mapStateToProps = ({ checkoutInfo, userInfo }) => {
  return {
    total: checkoutInfo.total,
    currency: checkoutInfo.currency,
    orderId: checkoutInfo.orderId,
    token: userInfo.token,
  }
};

class PayPalButton extends React.Component {

  onSuccess = (data) => {
    const { payment, nextStep } = this.props;
    this.payPalRequest('success', JSON.stringify(data));
    payment();
    nextStep();
  };

  onCancel = (data) => {
    const { nextStep } = this.props;
    nextStep();
    this.payPalRequest('canceled', JSON.stringify(data));
  };

  onError = (err) => {
    const { nextStep } = this.props;
    this.payPalRequest('failed', JSON.stringify(err));
    nextStep();
  };

  payPalRequest = (status, responseData) => {
    const { orderId, token } = this.props;
    axios.post(`order/payment/${orderId}`, {
      status,
      responseData
    }, {
      headers: {
        'AUTHORIZATION': `Bearer ${token}`
      }
    })
  };

  render() {
    const { currency, total } = this.props,
      env = 'sandbox',
      client = {
        sandbox: process.env.REACT_APP_PAYPAL_CLIENT_ID
      };

    return (
      <PaypalExpressBtn
        env={env}
        client={client}
        currency={currency}
        total={total}
        onError={this.onError}
        onSuccess={this.onSuccess}
        onCancel={this.onCancel}
        style={{
          color: 'silver',
          size: 'responsive',
          label: 'paypal',
          shape: 'rect'
        }}
      />
    )
  }
}

export default connect(mapStateToProps)(PayPalButton)
