import React from 'react';
import ProductsCatalog from './ProductsCatalog';
import Pagination from './Pagination';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { changeProductsInfo, updatePaginationParams } from 'actions';
import axios from 'axios';
import './style.scss';
import { Container } from 'react-bootstrap';
import Helmet from 'react-helmet';

const mapStateToProps = ({ menuInfo, cardsInfo }) => {
  return {
    total: cardsInfo.total,
    totalPages: cardsInfo.totalPages,
    perPage: cardsInfo.perPage,
    pageNumbersArray: cardsInfo.pageNumbersArray,
    page: cardsInfo.page,
    search: menuInfo.search,
    category: menuInfo.category,
    department: menuInfo.department
  }
};

class ProductsCatalogPage extends React.Component {

  componentDidMount() {
    const { department, category, search, perPage, config } = this.props,
      activePage = parseInt(config.match.params.page_no);
    this.getProductsData({
      department,
      category,
      search,
      page: activePage,
      perPage
    })
  }

  getProductsData = (params) => {
    const { dispatch } = this.props;
    axios.get('products/filter', { params })
      .then(resp => {
        const totalPages = Math.ceil(resp.data.totalRecords / params.perPage);
        dispatch(changeProductsInfo({ data: resp.data.list }));
        dispatch(updatePaginationParams({
          total: resp.data.totalRecords,
          perPage: params.perPage,
          page: params.page,
          totalPages
        }));
        this.makePaginationHref(totalPages, params.page);
      })
  };


  makePaginationHref = (pages, active) => {
    const { dispatch } = this.props,
      left = (
        <span>
        <Icon type="left" className="mr-1"/>Back
      </span>
      ),
      right = (
        <span>
          Next<Icon type="right" className="ml-1"/>
        </span>
      );
    let pageNumbersArray = [],
      startIndex, endIndex;
    if (pages <= 10) {
      startIndex = 1;
      endIndex = pages;
    } else {
      if (active <= 6) {
        startIndex = 1;
        endIndex = 10;
      } else if (active + 4 >= pages) {
        startIndex = pages - 9;
        endIndex = pages;
      } else {
        startIndex = active - 5;
        endIndex = active + 4;
      }
    }

    if (startIndex > 1) {
      pageNumbersArray.push({
        pageNo: (active - 1),
        view: left,
        className: 'pager-pagination-item-prev',
        classNameLink: '',
        key: 1
      });
    }

    for (let i = startIndex; i <= endIndex; i++) {
      pageNumbersArray.push({
        pageNo: i,
        view: i,
        className: active === i ? 'pager-pagination-item-active' : '',
        classNameLink: active === i ? 'pager-pagination-active-link' : '',
        key: i
      });
    }

    if (endIndex < pages) {
      pageNumbersArray.push({
        pageNo: (active + 1),
        view: right,
        className: 'pager-pagination-item-next',
        classNameLink: '',
        key: pages
      });
    }

    dispatch(changeProductsInfo({pageNumbersArray}));
  };

  changePageNumber = (page) => {
    const { department, category, search, perPage } = this.props;
    this.getProductsData({
      department,
      category,
      search,
      page,
      perPage
    })
  };

  render() {
    const { pageNumbersArray } = this.props;

    return (
      <div>
        <Helmet title="Products"/>
        <Container
          fluid={true}
          className="products"
        >
          <Pagination
            pageNumbersArray={pageNumbersArray}
            changePageNumber={this.changePageNumber}
          />
          <ProductsCatalog/>
          <Pagination
            pageNumbersArray={pageNumbersArray}
            changePageNumber={this.changePageNumber}
          />
        </Container>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ProductsCatalogPage)