<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductAdmin extends AbstractAdmin
{
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('discountedPrice')
            ->add('display')
            ->add('image')
            ->add('image2')
            ->add('thumbnail')
        ;
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('discountedPrice')
            ->add('display')
            ->add('categories')
            ->add('attributeValues')
            ->add('imageFile', VichImageType::class, [
                'allow_delete'  => true,
                'download_link' => false,
                'required' => false,
            ])
            ->add('image2File', VichImageType::class, [
                'allow_delete'  => true,
                'download_link' => false,
                'required' => false,
            ])
            ->add('thumbnailFile', VichImageType::class, [
                'allow_delete'  => true,
                'download_link' => false,
                'required' => false,
            ])
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('description')
            ->add('display')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('discountedPrice')
            ->add('display')

            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }
}
