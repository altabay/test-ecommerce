<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttributeRepository")
 */
class Attribute
{
    use IdTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var AttributeValue[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\AttributeValue",
     *     mappedBy="attribute",
     *     cascade={"persist","remove"},
     *     orphanRemoval=true
     * )
     */
    private $attributeValues;

    public function __construct()
    {
        $this->attributeValues = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->name;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return AttributeValue[]|ArrayCollection|Collection
     */
    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    /**
     * @param AttributeValue $value
     *
     * @return $this
     */
    public function addAttributeValue($value)
    {
        if (!$this->attributeValues->contains($value)) {
            $this->attributeValues->add($value);
        }

        return $this;
    }

    /**
     * @param AttributeValue $value
     *
     * @return $this
     */
    public function removeAttributeValue($value)
    {
        if ($this->attributeValues->contains($value)) {
            $this->attributeValues->removeElement($value);
        }

        return $this;
    }
}
