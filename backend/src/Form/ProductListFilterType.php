<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Department;
use App\Filter\ProductListFilter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductListFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search')
            ->add('department', EntityType::class, [
                'class' => Department::class,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
            ])
            ->add('page', IntegerType::class)
            ->add('perPage', IntegerType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductListFilter::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}
