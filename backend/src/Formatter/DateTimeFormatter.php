<?php

namespace App\Formatter;

class DateTimeFormatter
{
    /**
     * @var string
     */
    private $format;

    /**
     * @param string $format
     */
    public function __construct(
        string $format
    ) {
        $this->format = $format;
    }

    /**
     * @param \DateTime|null $dateTime
     *
     * @return string
     */
    public function format(?\DateTime $dateTime): string
    {
        return $dateTime
            ? $dateTime->format($this->format)
            : ''
        ;
    }
}
