import React from 'react';
import { Col, Form } from 'react-bootstrap';
import SocialLogin from '../SocialLogin';
import SmallButton from '../SmallButton';
import axios from 'axios';
import { registerOrLoginUser, changeStep } from 'actions';
import { connect } from 'react-redux';
import { notification } from 'antd';

class SignUpTab extends React.Component {
  state = {
    email: '',
    password: '',
    validated: false,
  };

  changeData = (name, e) => {
    this.setState({
      [name]: e.target.value
    })
  };

  openErrorNotification = (message) => {
    notification.error({
      message: message
    });
  };

  validate = (e) => {
    const form = e.currentTarget;
    e.preventDefault();
    e.stopPropagation();
    if (form.checkValidity()) {
      this.signUp();
    } else {
      this.setState({ validated: true });
    }
  };

  signUp = () => {
    const { dispatch, success } = this.props;
    const { email, password } = this.state;
    axios.post('user/register', {
      email,
      password
    })
      .then(resp => {
        dispatch(registerOrLoginUser({
          token: resp.data.token,
          email: resp.data.email
        }));
        dispatch(changeStep(1));
        success();
      })
  };

  render() {
    const { validated } = this.state;
    const { success } = this.props;

    return (
      <div className="my-3">
        <Form
          onSubmit={e => this.validate(e)}
          noValidate
          validated={validated}
        >
          <Form.Row className="justify-content-center">
            <Form.Group as={Col} xs={12}>
              <Form.Control
                required
                type="email"
                placeholder="Email"
                onChange={e => this.changeData('email', e)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid email.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} xs={12}>
              <Form.Control
                required
                type="password"
                placeholder="Password"
                onChange={e => this.changeData('password', e)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} xs={5} className="text-center">
              <SmallButton
                text="Sign up"
                type="submit"
              />
            </Form.Group>
          </Form.Row>
        </Form>
        <SocialLogin
          hideModal={success}
          type="Sign Up"
        />
      </div>
    )
  }
}

export default connect()(SignUpTab)
