<?php

namespace App\User\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserManagerInterface
     */
    private $fosUserManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @param UserRepository $userRepository
     * @param UserManagerInterface $fosUserManager
     * @param TokenStorageInterface $tokenStorage
     * @param EventDispatcherInterface $eventDispatcher
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(
        UserRepository $userRepository,
        UserManagerInterface $fosUserManager,
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $eventDispatcher,
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->userRepository = $userRepository;
        $this->fosUserManager = $fosUserManager;
        $this->tokenStorage = $tokenStorage;
        $this->eventDispatcher = $eventDispatcher;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function register(
        string $email,
        string $password
    ) {
        if ($this->userRepository->findUserByUsernameOrEmail($email)) {
            throw new \LogicException('Email already exists.');
        }

        $user = (new User())
            ->setEmail($email)
            ->setUsername($email)
            ->setEnabled(true)
            ->setPlainPassword($password)
        ;
        $this->fosUserManager->updateUser($user);

        return $this->loginFOSUser($user, $password);
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function login(string $email, string $password): User
    {
        $user = $this->userRepository->findUserByUsernameOrEmail($email);

        if (!$user) {
            throw new \LogicException('Email doesn\'t exist.');
        }

        $encoder = $this->encoderFactory->getEncoder($user);

        if (!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            throw new \LogicException('Password doesn\'t match.');
        }

        return $this->loginFOSUser($user, $password);
    }

    /**
     * @param string $email
     *
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function google(string $email): User
    {
        $user = $this->userRepository->findUserByUsernameOrEmail($email);

        if (!$user) {
            $user = (new User())
                ->setEmail($email)
                ->setUsername($email)
                ->setEnabled(true)
                ->setPlainPassword(md5(random_int(100000, 999999)))
            ;
            $this->fosUserManager->updateUser($user);
        }

        return $this->loginFOSUser($user, '-');
    }

    /**
     * @param User $user
     * @param string $password
     *
     * @return User
     */
    private function loginFOSUser(User $user, string $password): User
    {
        $providerKey = 'firewall_name'; // e.g main
        $token = new UsernamePasswordToken($user, $password, $providerKey, $user->getRoles());
        $this->tokenStorage->setToken($token);

        $event = new InteractiveLoginEvent(new Request(), $token);
        $this->eventDispatcher->dispatch(SecurityEvents::INTERACTIVE_LOGIN, $event);

        return $user;
    }
}
