const initialState = {
  data: [],
  page: 1,
  perPage: 12,
  total: 0,
  totalPages: 0,
  pageNumbersArray: []
};

const cardsInfo = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_PRODUCTS_INFO':
      return { ...state, ...action.params };
    case 'UPDATE_PAGINATION_PARAMS':
      return { ...state,
        ...action.param
      };
    default:
      return state
  }
};

export default cardsInfo