<?php

namespace App\Controller\Api;

use App\Controller\Traits\CreateNamelessFormTrait;
use App\DataObject\OrderDataObject;
use App\DataObject\PaymentDataObject;
use App\Entity\Cart;
use App\Entity\Order;
use App\Form\OrderType;
use App\Form\PaymentType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/order"
 * )
 */
class OrderController extends BaseFOSRestController
{
    use CreateNamelessFormTrait;

    /**
     * @Rest\Route(
     *     path="/create",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function createAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->errorView(Response::HTTP_UNAUTHORIZED, 'Unauthorized.');
        }

        $form = $this->createNamelessForm(OrderType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        /** @var OrderDataObject $orderDataObject */
        $orderDataObject = $form->getData();
        $carts = $this->getDoctrine()->getRepository(Cart::class)->findBy([
            'cartId' => $orderDataObject->getCartId(),
        ]);

        if (!count($carts)) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Empty cart.');
        }

        $order = $this
            ->get('app.order.order_converter')
            ->dataToOrder(
                $orderDataObject,
                $this->getUser(),
                $carts
            )
        ;
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);

        foreach ($carts as $cart) {
            $em->remove($cart);
        }

        $em->flush();

        return $this->view(
            $this->get('app.pack.order_pack')->pack($order),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/get/{id}",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function getAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->errorView(Response::HTTP_UNAUTHORIZED, 'Unauthorized.');
        }

        $order = $this->getDoctrine()->getRepository(Order::class)->find($request->get('id'));

        if (!$this->isOrderAvailable($order)) {
            return $this->errorView(Response::HTTP_NOT_FOUND, 'Order was not found.');
        }

        return $this->view(
            $this->get('app.pack.order_pack')->pack($order),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/payment/{id}",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     * @throws \Exception
     */
    public function paymentAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->errorView(Response::HTTP_UNAUTHORIZED, 'Unauthorized.');
        }

        $order = $this->getDoctrine()->getRepository(Order::class)->find($request->get('id'));

        if (!$this->isOrderAvailable($order)) {
            return $this->errorView(Response::HTTP_NOT_FOUND, 'Order was not found.');
        }

        $form = $this->createNamelessForm(PaymentType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        $this->get('app.order.payment_adder')->add($order, $form->getData());
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->view(
            '',
            Response::HTTP_OK
        );
    }

    /**
     * @param Order|null $order
     *
     * @return bool
     */
    private function isOrderAvailable(?Order $order): bool
    {
        return $order &&
            $order->getUser() &&
            ($order->getUser() !== $this->getUser()->getId())
        ;
    }
}
