import React from 'react';
import Title from 'components/CustomComponents/Title';

const Payment = ({ productsPrice, shippingPrice, totalPrice }) => {
  return (
    <div className="checkout-items-confirm-total d-flex justify-content-end">
      <div className="checkout-items-confirm-total-item">
        <Title text="Subtotal"/>
        <span>{productsPrice}</span>
      </div>
      <div className="checkout-items-confirm-total-item">
        <Title text="Shipping"/>
        <span>{shippingPrice}</span>
      </div>
      <div className="checkout-items-confirm-total-item">
        <Title text="Grandtotal"/>
        <span>{totalPrice}</span>
      </div>
    </div>
  )
};

export default Payment