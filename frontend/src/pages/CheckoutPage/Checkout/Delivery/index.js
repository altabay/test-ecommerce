import React from 'react';
import { Form } from 'react-bootstrap';
import SmallButton from 'components/CustomComponents/SmallButton';
import UserInfo from './UserInfo';
import Shipping from './Shipping';
import axios from 'axios';
import { connect } from 'react-redux';
import { changeCartId, updateOrdersData, updateOrdersParams, changeCartData } from 'actions';

const mapStateToProps = ({ cartInfo, userInfo }) => {
  return {
    cartId: cartInfo.cartId,
    token: userInfo.token,
  }
};

class Delivery extends React.Component {
  state = {
    validated: false,
  };

  validate = (e) => {
    const form = e.currentTarget;
    if (form.checkValidity()) {
      this.createOrder();
    } else {
      this.setState({ validated: true });
    }
    e.preventDefault();
    e.stopPropagation();
  };

  createOrder = () => {
    const { deliveryData, cartId, token, dispatch, nextStep } = this.props,
      newDeliveryData = { ...deliveryData };
    delete newDeliveryData.region;
    axios.post('order/create', {
      cartId,
      ...newDeliveryData
    }, {
      headers: {
        'AUTHORIZATION': `Bearer ${token}`
      }
    })
      .then(resp => {
        dispatch(changeCartId(null));
        dispatch(updateOrdersParams({
          productsPrice: resp.data.productsPriceFormatted,
          shippingPrice: resp.data.shippingPriceFormatted,
          totalPrice: resp.data.totalPriceFormatted,
          total: resp.data.totalPrice,
          shippingDescription: resp.data.shippingDescription,
          orderId: resp.data.id,
          currency: resp.data.currency,
        }));
        dispatch(updateOrdersData(resp.data.items));
        dispatch(changeCartData([]))
        nextStep();
      })
  };

  render() {
    const { changeDeliveryData, deliveryData } = this.props,
      { validated } = this.state;

    return (
      <div className="checkout-items-delivery">
        <Form
          onSubmit={this.validate}
          noValidate
          validated={validated}
        >
          <UserInfo
            changeDeliveryData={changeDeliveryData}
            deliveryData={deliveryData}
          />
          <Shipping
            changeDeliveryData={changeDeliveryData}
            deliveryData={deliveryData}
          />
          <Form.Row className="justify-content-center checkout-buttons">
            <SmallButton
              text="Next"
              type="submit"
            />
          </Form.Row>
        </Form>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Delivery)