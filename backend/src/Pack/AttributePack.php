<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 07.03.19
 * Time: 12:12
 */

namespace App\Pack;


use App\Entity\Attribute;

class AttributePack
{
    /**
     * @var AttributeValuePack
     */
    private $attributeValuePack;

    /**
     * @param AttributeValuePack $attributeValuePack
     */
    public function __construct(AttributeValuePack $attributeValuePack)
    {
        $this->attributeValuePack = $attributeValuePack;
    }

    /**
     * @param Attribute $attribute
     * @param array $attributeValues
     *
     * @return array
     */
    public function pack(Attribute $attribute, array $attributeValues): array
    {
        return [
            'id' => $attribute->getId(),
            'name' => (string)$attribute->getName(),
            'values' => $this->attributeValuePack->packList($attributeValues),
        ];
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    public function packList(array $attributes): array
    {
        $list = [];

        foreach ($attributes as $attribute) {
            $list[] = $this->pack($attribute['attribute'], $attribute['values']);
        }

        return $list;
    }
}