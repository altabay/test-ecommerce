import React from 'react';
import { Button } from 'antd';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { connect } from 'react-redux';
import { changeCartData, changeCartTotalPrice } from 'actions';

const mapStateToProps = ({ cartInfo }) => {
  return {
    cartId: cartInfo.cartId,
  }
};

class MenuCartItem extends React.Component {

  removeItem = (id) => {
    const { cartId, dispatch } = this.props;

    axios.post('cart/remove',
      {
        cart: id,
        cartId
      }
    )
      .then(resp => {
        dispatch(changeCartData(resp.data.items));
        dispatch(changeCartTotalPrice(resp.data.totalFormatted));
      })
      .catch(err => {
        console.log(err)
      })
  };

  render() {
    const { cartItem } = this.props;
    return (
      <div className='cart-item'>
        <Row>
          <Col xs={9}>
            <div className='cart-item-title'>
              {cartItem.amount}
              <span className='mx-2 cart-item-title-icon'>x</span>
              <span>{cartItem.product.name}</span>
            </div>
            <div className='cart-item-attributes'>
              {cartItem.attributes.map(attribute =>
                <h4 key={attribute.attributeId}>{attribute.attributeName}: {attribute.attributeValue}</h4>
              )}
            </div>
          </Col>
          <Col xs={3} className='d-flex justify-content-end'>
            <Button
              onClick={() => this.removeItem(cartItem.cartProductId)}
              htmlType='button'
              shape='circle'
              icon='close'
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(mapStateToProps)(MenuCartItem)