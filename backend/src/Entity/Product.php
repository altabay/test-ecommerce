<?php

namespace App\Entity;

use App\Entity\Traits\CreatedUpdatedTrait;
use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @Vich\Uploadable
 */
class Product
{
    use IdTrait;
    use CreatedUpdatedTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=10, scale=2, options={"default": 0.00})
     */
    private $discountedPrice;

    /**
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="image")
     *
     * @Assert\Image(
     *     maxSize="10M"
     * )
     */
    private $imageFile;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_image2", fileNameProperty="image2")
     *
     * @Assert\Image(
     *     maxSize="10M"
     * )
     */
    private $image2File;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $image2;

    /**
     * @Vich\UploadableField(mapping="product_thumbnail", fileNameProperty="thumbnail")
     *
     * @Assert\Image(
     *     maxSize="10M"
     * )
     */
    private $thumbnailFile;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $thumbnail;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $display;

    /**
     * @var AttributeValue[]|Collection
     *
     * @ORM\ManyToMany(
     *     targetEntity="App\Entity\AttributeValue"
     * )
     * @ORM\JoinTable(
     *   name="product_attribute",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="attribute_value_id", referencedColumnName="id")
     *   }
     * )
     */
    private $attributeValues;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var Category[]|Collection
     *
     * @ORM\ManyToMany(
     *     targetEntity="App\Entity\Category"
     * )
     * @ORM\JoinTable(
     *   name="product_category",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *   }
     * )
     */
    private $categories;

    private function __construct()
    {
        $this->display = false;
        $this->attributeValues = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setDescription(string $value): self
    {
        $this->description = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setPrice(float $value): self
    {
        $this->price = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountedPrice(): ?float
    {
        return $this->discountedPrice;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setDiscountedPrice(?float $value): self
    {
        $this->discountedPrice = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getFinalPrice(): float
    {
        return $this->getDiscountedPrice() > 0
            ? $this->getDiscountedPrice()
            : $this->getPrice()
        ;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setImage(?string $value): self
    {
        $this->image = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage2(): ?string
    {
        return $this->image2;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setImage2(?string $value): self
    {
        $this->image2 = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setThumbnail(?string $value): self
    {
        $this->thumbnail = $value;

        return $this;
    }

    /**
     * @param AttributeValue $value
     *
     * @return $this
     */
    public function addAttributeValue(AttributeValue  $value)
    {
        if (!$this->attributeValues->contains($value)) {
            $this->attributeValues->add($value);
        }

        return $this;
    }

    /**
     * @param AttributeValue $value
     *
     * @return $this
     */
    public function removeAttributeValue(AttributeValue $value)
    {
        if ($this->attributeValues->contains($value)) {
            $this->attributeValues->removeElement($value);
        }

        return $this;
    }

    /**
     * @return AttributeValue[]|Collection
     */
    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    /**
     * @param AttributeValue[]|Collection $value
     *
     * @return $this
     */
    public function setAttributeValues($value): self
    {
        $this->attributeValues = $value;

        return $this;
    }

    public function getAttributes()
    {
        if ($this->attributes !== null) {
            return $this->attributes;
        }

        $this->attributes = [];

        foreach ($this->attributeValues as $attributeValue) {
            if (!isset($this->attributes[$attributeValue->getAttribute()->getId()])) {
                $this->attributes[$attributeValue->getAttribute()->getId()] = [
                    'attribute' => $attributeValue->getAttribute(),
                    'values' => [$attributeValue],
                ];

                continue;
            }

            $this->attributes[$attributeValue->getAttribute()->getId()]['values'][] = $attributeValue;
        }

        return $this->attributes;
    }

    /**
     * @param Category $value
     *
     * @return $this
     */
    public function addCategory(Category  $value)
    {
        if (!$this->categories->contains($value)) {
            $this->categories->add($value);
        }

        return $this;
    }

    /**
     * @param Category $value
     *
     * @return $this
     */
    public function removeCategory(Category $value)
    {
        if ($this->categories->contains($value)) {
            $this->categories->removeElement($value);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function getDisplay(): bool
    {
        return $this->display;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function setDisplay(bool $value): self
    {
        $this->display = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setImageFile(?File $value = null): self
    {
        $this->imageFile = $value;

        if ($this->imageFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage2File(): ?File
    {
        return $this->image2File;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setImage2File(?File $value = null): self
    {
        $this->image2File = $value;

        if ($this->image2File instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnailFile(): ?File
    {
        return $this->thumbnailFile;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setThumbnailFile(?File $value = null): self
    {
        $this->thumbnailFile = $value;

        if ($this->thumbnailFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return Category[]|Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[]|Collection $value
     *
     * @return $this
     */
    public function setCategories($value): self
    {
        $this->categories = $value;

        return $this;
    }
}
