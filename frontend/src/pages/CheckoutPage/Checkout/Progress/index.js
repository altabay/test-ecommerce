import React from 'react';
import { Steps } from 'antd';

const Step = Steps.Step;

const Progress = ({step}) => {
  return (
    <Steps current={step} size="small">
      <Step title="Auth"/>
      <Step title="Delivery"/>
      <Step title="Confirm"/>
      <Step title="Payment"/>
      <Step title="Finish"/>
    </Steps>
  )
};

export default Progress