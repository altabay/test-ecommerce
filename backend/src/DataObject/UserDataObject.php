<?php

namespace App\DataObject;

use Symfony\Component\Validator\Constraints as Assert;


class UserDataObject
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setEmail(string $value): self
    {
        $this->email = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPassword(string $value): self
    {
        $this->password = $value;

        return $this;
    }


}
