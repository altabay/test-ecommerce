<?php

namespace App\Controller\Api;

use App\Entity\ShippingRegion;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/shipping"
 * )
 */
class ShippingController extends FOSRestController
{
    /**
     * @Rest\Route(
     *     path="/all",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function allAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $list = $this
            ->getDoctrine()
            ->getRepository(ShippingRegion::class)
            ->findAll()
        ;

        return $this->view(
            $this->get('app.pack.shipping_region_pack')->packList($list),
            Response::HTTP_OK
        );
    }
}
