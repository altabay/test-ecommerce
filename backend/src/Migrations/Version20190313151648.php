<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190313151648 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("
            INSERT INTO `shipping_region` (`id`, `name`) VALUES
                (1, 'US / Canada'),
                (2, 'Europe'),
                (3, 'Rest of World');
        ");
        $this->addSql("
        INSERT INTO `shipping` (`shipping_type`, `shipping_cost`, `shipping_region_id`) VALUES
            ('Next Day Delivery ($20)', 20.00, 1),
            ('3-4 Days ($10)', 10.00, 1),
            ('7 Days ($5)', 5.00, 1),
            ('By air (7 days, $25)', 25.00, 2),
            ('By sea (28 days, $10)', 10.00, 2),
            ('By air (10 days, $35)', 35.00, 3),
            ('By sea (28 days, $30)', 30.00, 3);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
    }
}
