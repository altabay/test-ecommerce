import {Provider} from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import configureStore from './configureStore';
import * as serviceWorker from './serviceWorker';
import {createBrowserHistory} from 'history';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

import './index.scss';
import './resources/AntStyles/AntDesign/antd.custom.scss';
import './resources/Bootstrap/BootstrapDesign/bootstrap.custom.scss';

const store = configureStore();
export const history = createBrowserHistory();


ReactDOM.render(
  <Provider store={store}>
    <App history={history}/>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.register();


