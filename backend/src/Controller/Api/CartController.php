<?php

namespace App\Controller\Api;


use App\Controller\Traits\CreateNamelessFormTrait;
use App\DataObject\CartAddProductDataObject;
use App\DataObject\CartAmountProductDataObject;
use App\DataObject\CartProductDataObject;
use App\Form\CartAddProductType;
use App\Form\CartAmountProductType;
use App\Form\CartDeleteProductType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/cart"
 * )
 */
class CartController extends BaseFOSRestController
{
    use CreateNamelessFormTrait;

    /**
     * @Rest\Route(
     *     path="/get/{cartId}",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     * @param $cartId
     *
     * @return View
     */
    public function getAction(ParamFetcherInterface $paramFetcher, Request $request, $cartId)
    {
        return $this->view(
            $this->get('app.pack.cart_pack')->pack($cartId),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/add",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function addProductAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $cartAddProduct = new CartAddProductDataObject();
        $form = $this->createNamelessForm(CartAddProductType::class, $cartAddProduct);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        try {
            $cart = $this->get('app.cart.cart_product_adder')->add(
                $cartAddProduct->getCartId(),
                $cartAddProduct->getProduct(),
                $cartAddProduct->getAmount(),
                $cartAddProduct->getAttributeValues()
            );
        } catch (\Exception $e) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, $e->getMessage(), $form);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($cart);
        $em->flush();

        return $this->view(
            $this->get('app.pack.cart_pack')->pack($cart->getCartId()),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/remove",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function removeAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $cartProduct = new CartProductDataObject();
        $form = $this->createNamelessForm(CartDeleteProductType::class, $cartProduct);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        if ($cartProduct->getCart()->getCartId() === $cartProduct->getCartId()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cartProduct->getCart());
            $em->flush();
        }

        return $this->view(
            $this->get('app.pack.cart_pack')->pack($cartProduct->getCartId()),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Route(
     *     path="/amount",
     *     methods={"POST"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function updateAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $cartAmountProduct = new CartAmountProductDataObject();
        $form = $this->createNamelessForm(CartAmountProductType::class, $cartAmountProduct);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->errorView(Response::HTTP_BAD_REQUEST, 'Invalid fields.', $form);
        }

        if ($cartAmountProduct->getCart()->getCartId() === $cartAmountProduct->getCartId()) {
            $cartAmountProduct->getCart()->setAmount($cartAmountProduct->getAmount());
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->view(
            $this->get('app.pack.cart_pack')->pack($cartAmountProduct->getCartId()),
            Response::HTTP_OK
        );
    }
}
