import React from 'react';
import { InputNumber, Table, Button } from 'antd';
import './style.scss';
import { changeCartData, changeCartTotalPrice } from 'actions';
import { connect } from 'react-redux';
import axios from 'axios';

const mapStateToProps = ({ cartInfo }) => {
  return {
    cartId: cartInfo.cartId,
    totalPrice: cartInfo.totalPrice,
    cartData: cartInfo.cartData,
  }
};

class CartList extends React.Component {
  state = {
    dataSource: []
  };

  componentDidMount() {
    this.generateNewTableData();
  }

  generateNewTableData = () => {
    const { cartData } = this.props;
    let tableData = [];
    cartData.map(item => {
      tableData.push({
        item: item.product.name,
        price: item.totalFormatted,
        options: item.attributes,
        key: item.cartProductId,
        quantity: item.amount,
        image: item.product.thumbnail
      });
      return true
    });
    this.setState({
      dataSource: tableData
    })
  };


  removeItem = (e, id) => {
    e.stopPropagation();
    const { cartId, dispatch } = this.props;

    axios.post('cart/remove',
      {
        cart: id,
        cartId
      }
    )
      .then(resp => {
        dispatch(changeCartData(resp.data.items));
        dispatch(changeCartTotalPrice(resp.data.totalFormatted));
        this.generateNewTableData();
      })
  };

  changeAmount = (value, id) => {
    const { cartId, dispatch } = this.props;

    axios.post('cart/amount',
      {
        cart: id,
        cartId,
        amount: value
      }
    )
      .then(resp => {
        dispatch(changeCartData(resp.data.items));
        dispatch(changeCartTotalPrice(resp.data.totalFormatted));
        this.generateNewTableData();
      })
  };

  render() {
    const { dataSource } = this.state;
    const { totalPrice } = this.props;

    const columns = [{
      title: 'Item',
      dataIndex: 'item',
      key: 'item',
      align: 'center',
      width: '20%',
      render: (text, record) => {
        return (
          <div className="cart-list-item">
            <img src={record.image} alt=""/>
            <span>{record.item}</span>
          </div>
        )
      }
    }, {
      title: 'Options',
      dataIndex: 'options',
      key: 'options',
      align: 'center',
      width: '30%',
      render: (text, record) => {
        return record.options.map(option =>
          <p key={option.attributeId}>
            {option.attributeName}: {option.attributeValue}
          </p>
        )
      }
    }, {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'quantity',
      align: 'center',
      width: '20%',
      render: (text, record) => {
        return (
          <InputNumber
            min={1}
            value={record.quantity}
            onChange={(value) => this.changeAmount(value, record.key)}
          />
        )
      }
    }, {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      width: '15%',
    }, {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      align: 'center',
      width: '15%',
      render: (text, record) => {
        return (
          <Button
            onClick={(e) => this.removeItem(e, record.key)}
            htmlType='button'
            shape='circle'
            icon='close'
          />
        )
      },
    }];

    return (
      <div className="cart-list">
        <Table
          dataSource={dataSource}
          columns={columns}
          pagination={false}
          scroll={{ y: 500, x: 700 }}
        />
        <div className="text-right total-price">
          Total Price:
          <span className="total-price-value">{totalPrice}</span>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(CartList)