<?php

namespace App\Product;

use App\Entity\AttributeValue;
use App\Entity\Product;

class ProductAttributesValidate
{
    /**
     * @param Product $product
     * @param array|AttributeValue[] $attributeValues
     *
     * @return bool
     * @throws \RuntimeException
     */
    public function validateAttributes(Product $product, array $attributeValues): bool
    {
        $originalAttributeIds = $product->getAttributes();

        foreach ($attributeValues as $attributeValue) {
            if (!isset($originalAttributeIds[$attributeValue->getAttribute()->getId()])) {
                throw new \RuntimeException(
                    sprintf(
                        'Extra value "%s:%s" specified.',
                        $attributeValue->getAttribute()->getName(),
                        $attributeValue->getValue()
                    )
                );
            }

            unset($originalAttributeIds[$attributeValue->getAttribute()->getId()]);
        }

        if (count($originalAttributeIds)) {
            throw new \RuntimeException('Some attribute\'s value(s) is(are) missing.');
        }

        return true;
    }
}
