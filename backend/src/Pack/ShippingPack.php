<?php

namespace App\Pack;

use App\Entity\Shipping;
use App\Formatter\PriceFormatter;

class ShippingPack
{
    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @param PriceFormatter $priceFormatter
     */
    public function __construct(
        PriceFormatter $priceFormatter
    ) {
        $this->priceFormatter = $priceFormatter;
    }

    /**
     * @param Shipping $shipping
     *
     * @return array
     */
    public function pack(Shipping $shipping): array
    {
        return [
            'id' => $shipping->getId(),
            'type' => (string)$shipping->getShippingType(),
            'cost' => (string)$shipping->getShippingCost(),
            'costFormatted' => $this->priceFormatter->format($shipping->getShippingCost()),
        ];
    }

    /**
     * @param array|Shipping[] $shippings
     *
     * @return array
     */
    public function packList(array $shippings): array
    {
        $list = [];

        foreach ($shippings as $shipping) {
            $list[] = $this->pack($shipping);
        }

        return $list;
    }
}
