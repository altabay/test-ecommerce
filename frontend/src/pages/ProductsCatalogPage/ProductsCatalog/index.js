import React from 'react';
import Products from './Products';
import MenuLeft from 'components/CustomComponents/MenuLeft';
import { Col, Row } from 'react-bootstrap';

const ProductsCatalog = () => {
  return (
    <Row>
      <Col xs={12} lg={3} xl={2}>
        <MenuLeft/>
      </Col>
      <Col>
        <Products/>
      </Col>
    </Row>
  )
};


export default ProductsCatalog