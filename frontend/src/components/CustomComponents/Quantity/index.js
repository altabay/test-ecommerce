import React from 'react';
import './style.scss';
import { InputNumber } from 'antd';

const Quantity = ({ amount, changeAmount }) => {
  return (
    <InputNumber
      min={1}
      value={amount}
      onChange={changeAmount}
    />
  )
};

export default Quantity