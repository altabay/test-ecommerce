<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;

/**
 * {@inheritdoc}
 */
class BaseFOSRestController extends FOSRestController
{
    /**
     * @param int $statusCode
     * @param string $message
     * @param FormInterface|null $form
     *
     * @return View
     */
    protected function errorView(int $statusCode, string $message, FormInterface $form = null): View
    {
        if ($form && ($errorText = $form->getErrors(true, false))) {
            $message .= "\n" . $errorText;
        }

        return $this->view(
            [
                'code' => $statusCode,
                'message' => $message,
            ],
            $statusCode
        );
    }
}
