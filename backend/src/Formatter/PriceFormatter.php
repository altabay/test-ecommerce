<?php

namespace App\Formatter;

class PriceFormatter
{
    /**
     * @var string
     */
    private $priceFormat;

    /**
     * @param string $priceFormat
     */
    public function __construct(string $priceFormat)
    {
        $this->priceFormat = $priceFormat;
    }

    /**
     * @param float|null $price
     *
     * @return string
     */
    public function format(?float $price)
    {
        return sprintf($this->priceFormat, (float)$price);
    }
}
