import React from 'react';
import PayPalButton from 'components/CustomComponents/PayPalButton';

const Payment = ({ nextStep, payment }) => {
  return (
    <div className="checkout-items-payment">
      <PayPalButton
        nextStep={nextStep}
        payment={payment}
      />
    </div>
  )
};

export default Payment