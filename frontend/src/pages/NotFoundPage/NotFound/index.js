import React from 'react';
import './style.scss';
import { Container } from 'react-bootstrap';
import { Icon } from 'antd';
import Title from 'components/CustomComponents/Title';
import SmallButton from 'components/CustomComponents/SmallButton';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Container className="text-center my-5 not-found">
      <Title text="404! Page Not Found" className="not-found-title"/>
      <Icon type="warning" className="not-found-icon"/>
      <Link to="/">
        <SmallButton text="Back to shop"/>
      </Link>
    </Container>
  )
};

export default NotFound