<?php

namespace App\Pack;

use App\Entity\OrderProduct;
use App\Formatter\PriceFormatter;

class OrderProductPack
{
    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @param PriceFormatter $priceFormatter
     */
    public function __construct(
        PriceFormatter $priceFormatter
    ) {
        $this->priceFormatter = $priceFormatter;
    }

    /**
     * @param OrderProduct $orderProduct
     *
     * @return array
     */
    public function pack(OrderProduct $orderProduct): array
    {
        return [
            'id' => $orderProduct->getId(),
            'productName' => $orderProduct->getProductName(),
            'price' => $orderProduct->getProductPrice(),
            'priceFormatted' => $this->priceFormatter->format($orderProduct->getProductPrice()),
            'amount' => $orderProduct->getAmount(),
            'totalPrice' => $orderProduct->getProductPrice() * $orderProduct->getAmount(),
            'totalPriceFormatted' => $this->priceFormatter->format($orderProduct->getProductPrice() * $orderProduct->getAmount()),
            'attributes' => $orderProduct->getAttributes(),
        ];
    }

    /**
     * @param array|OrderProduct[] $orderProducts
     *
     * @return array
     */
    public function packList(array $orderProducts): array
    {
        $list = [];

        foreach ($orderProducts as $orderProduct) {
            $list[] = $this->pack($orderProduct);
        }

        return $list;
    }
}
