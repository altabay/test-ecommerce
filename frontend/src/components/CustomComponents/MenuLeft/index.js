import React from 'react';
import { Input, Icon } from 'antd';
import './style.scss';
import Filter from '../Filter';
import { Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import { withRouter } from "react-router-dom";
import {
  changeMenuInfo,
  changeProductsInfo,
  filterCategoriesInfo,
  updateMenuParams,
  updatePaginationParams
} from 'actions';
import MenuCart from '../MenuCart';

const mapStateToProps = ({ menuInfo, cardsInfo }) => {
  return {
    departments: menuInfo.departments,
    filterCategories: menuInfo.filterCategories,
    department: menuInfo.department,
    category: menuInfo.category,
    search: menuInfo.search,
    perPage: cardsInfo.perPage,
    page: cardsInfo.page,
    totalPages: cardsInfo.page,
  }
};

class MenuLeft extends React.Component {
  state = {
    updateParam: {},
  };

  componentDidMount() {
    const { dispatch } = this.props;
    axios.get('departments/all')
      .then(resp => {
        dispatch(changeMenuInfo(this.sortingData(resp.data)));
      })
  };

  sortingData = (data) => {
    const departments = [],
      categories = [];
    data.map(department => {
        departments.push(
          {
            id: department.id,
            name: department.name,
            description: department.description
          });
        department.categories.map(category => {
          categories.push({
            id: category.id,
            name: category.name,
            description: category.description,
            departmentId: department.id
          });
          return true
        });
        return true
      }
    );
    return { departments, categories }
  };

  chooseFilter = (index, value) => {
    const { dispatch, department, category } = this.props;
    if (index === 'department') {
      dispatch(filterCategoriesInfo(value));
      if (value === department) {
        this.changeParam({
          category: null,
          [index]: null
        });
      } else {
        this.changeParam({
          category: null,
          [index]: value
        });
      }
    } else if (index === 'category') {
      if (value === category) {
        this.changeParam({ [index]: null });
      } else {
        this.changeParam({ [index]: value });
      }
    } else {
      this.changeParam({ [index]: value });
    }
  };

  changeParam = (updateParam) => {
    const { dispatch } = this.props;
    dispatch(updateMenuParams(updateParam));
    this.prepareData(updateParam);
  };

  prepareData = (param) => {
    const { department, category, perPage, search } = this.props;
    this.getProductsData({
      department,
      category,
      page: 1,
      perPage,
      search,
      ...param
    })
  };

  getProductsData = (params) => {
    const { dispatch, history } = this.props;
    axios.get('products/filter', { params })
      .then(resp => {
        const totalPages = Math.ceil(resp.data.totalRecords / params.perPage);
        dispatch(changeProductsInfo({ data: resp.data.list }));
        dispatch(updatePaginationParams({
          total: resp.data.totalRecords,
          perPage: params.perPage,
          page: params.page,
          totalPages
        }));
        this.makePaginationHref(totalPages, params.page);
        history.push(`/page/${params.page}`)
      })
  };

  makePaginationHref = (pages, active) => {
    const { dispatch } = this.props,
      left = (
        <span>
        <Icon type="left" className="mr-1"/>Back
      </span>
      ),
      right = (
        <span>
          Next<Icon type="right" className="ml-1"/>
        </span>
      );
    let pageNumbersArray = [],
      startIndex, endIndex;
    if (pages <= 10) {
      startIndex = 1;
      endIndex = pages;
    } else {
      if (active <= 6) {
        startIndex = 1;
        endIndex = 10;
      } else if (active + 4 >= pages) {
        startIndex = pages - 9;
        endIndex = pages;
      } else {
        startIndex = active - 5;
        endIndex = active + 4;
      }
    }

    if (startIndex > 1) {
      pageNumbersArray.push({
        pageNo: (active - 1),
        view: left,
        className: 'pager-pagination-item-prev',
        classNameLink: '',
        key: 1
      });
    }

    for (let i = startIndex; i <= endIndex; i++) {
      pageNumbersArray.push({
        pageNo: i,
        view: i,
        className: active === i ? 'pager-pagination-item-active' : '',
        classNameLink: active === i ? 'pager-pagination-active-link' : '',
        key: i
      });
    }

    if (endIndex < pages) {
      pageNumbersArray.push({
        pageNo: (active + 1),
        view: right,
        className: 'pager-pagination-item-next',
        classNameLink: '',
        key: pages
      });
    }

    dispatch(changeProductsInfo({pageNumbersArray}));
  };

  render() {
    const { departments, filterCategories, department, category, search } = this.props;

    return (
      <Container fluid={true}>
        <Row className="menu-left">
          <Col xs={12}>
            <div className="menu-left-search">
              <Input
                placeholder="Search"
                allowClear
                value={search}
                onChange={(e) => this.chooseFilter('search', e.target.value)}
              />
            </div>
          </Col>
          <Col xs={12}>
            <Filter
              selected={department}
              chooseFilter={this.chooseFilter}
              text="Department"
              options={departments}
              index="department"
            />
          </Col>
          {department &&
          <Col xs={12}>
            <Filter
              selected={category}
              chooseFilter={this.chooseFilter}
              text="Category"
              options={filterCategories}
              index="category"
            />
          </Col>
          }
          <Col xs={12}>
            <MenuCart/>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default withRouter(connect(mapStateToProps)(MenuLeft))