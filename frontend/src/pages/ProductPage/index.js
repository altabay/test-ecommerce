import React from 'react';
import Helmet from 'react-helmet';
import ProductDetails from "./ProductDetails";

class ProductPage extends React.Component {

  render() {
    const props = this.props;

    return (
      <div>
        <Helmet title="Product"/>
        <ProductDetails {...props}/>
      </div>
    )
  }
}

export default ProductPage