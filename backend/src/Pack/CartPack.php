<?php

namespace App\Pack;

use App\Formatter\PriceFormatter;
use App\Repository\CartRepository;

class CartPack
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var CartItemPack
     */
    private $cartItemPack;

    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @param CartRepository $cartRepository
     * @param CartItemPack $cartItemPack
     * @param PriceFormatter $priceFormatter
     */
    public function __construct(
        CartRepository $cartRepository,
        CartItemPack $cartItemPack,
        PriceFormatter $priceFormatter
    ) {
        $this->cartRepository = $cartRepository;
        $this->cartItemPack = $cartItemPack;
        $this->priceFormatter = $priceFormatter;
    }

    /**
     * @param string $cartId
     *
     * @return array
     */
    public function pack(string $cartId): array
    {
        $packed = [
            'cartId' => $cartId,
            'total' => 0,
            'totalFormatted' => '',
            'shipping' => [],
            'items' => $this->cartItemPack->packList($this->getCarts($cartId)),
        ];

        foreach ($packed['items'] as $item) {
            $packed['total'] += $item['total'];
        }

        $packed['totalFormatted'] = $this->priceFormatter->format($packed['total']);

        return $packed;
    }

    /**
     * @param $cartId
     *
     * @return array
     */
    private function getCarts($cartId): array
    {
        return $this->cartRepository->findBy([
            'cartId' => $cartId,
        ], [
            'id' => 'ASC',
        ]);
    }
}
