<?php

namespace App\DataObject;

use App\Entity\Shipping;
use Symfony\Component\Validator\Constraints as Assert;

class OrderDataObject
{
    /**
     * @var string 32 chars cart id
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="32",
     *     min="32"
     * )
     */
    private $cartId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     */
    private $lastName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $state;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="10"
     * )
     */
    private $zip;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var Shipping
     *
     * @Assert\NotBlank()
     */
    private $shipping;

    /**
     * @return string
     */
    public function getCartId(): ?string
    {
        return $this->cartId;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCartId(string $value): self
    {
        $this->cartId = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setFirstName(string $value): self
    {
        $this->firstName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setLastName(string $value): self
    {
        $this->lastName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCountry(string $value): self
    {
        $this->country = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setState(string $value): self
    {
        $this->state = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setZip(string $value): self
    {
        $this->zip = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCity(string $value): self
    {
        $this->city = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setAddress(string $value): self
    {
        $this->address = $value;

        return $this;
    }

    /**
     * @return Shipping
     */
    public function getShipping(): ?Shipping
    {
        return $this->shipping;
    }

    /**
     * @param Shipping $value
     *
     * @return $this
     */
    public function setShipping(Shipping $value): self
    {
        $this->shipping = $value;

        return $this;
    }
}
