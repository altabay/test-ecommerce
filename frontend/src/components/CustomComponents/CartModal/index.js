import React from 'react';
import { Modal } from 'react-bootstrap';
import SmallButton from '../SmallButton';
import CartList from "./CartList";
import { Link } from "react-router-dom";

const CartModal = ({ show, toggleCartModal, showButton }) => {
  return (
    <Modal
      show={show}
      onHide={toggleCartModal}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton/>
      <Modal.Body>
        <CartList/>
      </Modal.Body>
      <Modal.Footer>
        {showButton &&
        <Link to="/checkout">
          <SmallButton text="Checkout"/>
        </Link>
        }
      </Modal.Footer>
    </Modal>
  )
};

export default CartModal