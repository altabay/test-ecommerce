import React from 'react';
import { Col, Form } from 'react-bootstrap';
import Title from 'components/CustomComponents/Title';
import { connect } from 'react-redux';
import { updateShippingData, filterTypesInfo } from 'actions';
import axios from 'axios';

const mapStateToProps = ({ checkoutInfo }) => {
  return {
    regions: checkoutInfo.regions,
    filterTypes: checkoutInfo.filterTypes,
  }
};

class Shipping extends React.Component {

  componentDidMount() {
    const { dispatch } = this.props;
    axios.get('shipping/all')
      .then(resp => {
        const data = this.sortingData(resp.data);
        dispatch(updateShippingData(data));
        if (data.regions.length) {
          this.filterTypes(data.regions[0].id, data.types)
        }
      })
  }

  sortingData = (data) => {
    const regions = [],
      types = [];
    data.map(region => {
        regions.push(
          {
            id: region.id,
            name: region.name
          });
        region.shippings.map(shipping => {
          types.push({
            id: shipping.id,
            type: shipping.type,
            cost: shipping.cost,
            costFormatted: shipping.costFormatted,
            regionId: region.id
          });
          return true
        });
        return true
      }
    );
    return { regions, types }
  };

  filterTypes = (value, types) => {
    const { dispatch, changeDeliveryData } = this.props;
    dispatch(filterTypesInfo(value));
    changeDeliveryData('region', value);
    const filterTypes = this.chooseDefaultType(value, types);
    if (filterTypes.length) {
      changeDeliveryData('shipping', filterTypes[0].id);
    }
  };

  chooseDefaultType = (id, data) => {
    return data.filter(item => item.regionId === id)
  };

  render() {
    const { changeDeliveryData, regions, filterTypes } = this.props;

    return (
      <Form.Row className="checkout-items-delivery-row">
        <Col xs={12}>
          <Title text="Shipping"/>
        </Col>
        <Form.Group as={Col} xs={12} md={6} xl={4}>
          <Form.Label>Region</Form.Label>
          <Form.Control
            as="select"
            onChange={e => this.filterTypes(e.target.value, filterTypes)}
          >
            {regions.map(region =>
              <option key={region.id} value={region.id}>{region.name}</option>
            )}
          </Form.Control>
        </Form.Group>
        <Form.Group as={Col} xs={12} md={6} xl={4}>
          <Form.Label>Type</Form.Label>
          <Form.Control
            onChange={e => changeDeliveryData('shipping', e.target.value)}
            as="select"
          >
            {filterTypes.map(type =>
              <option key={type.id} value={type.id}>{type.type}</option>
            )}
          </Form.Control>
        </Form.Group>
      </Form.Row>

    )
  }
}

export default connect(mapStateToProps)(Shipping)