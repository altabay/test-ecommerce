import React from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import routes from './routes';
import axios from 'axios';
import { notification } from 'antd';
import { logout } from 'actions';
import { connect } from 'react-redux';

class App extends React.Component {

  componentWillMount() {
    const { dispatch } = this.props;
    axios.defaults.baseURL = process.env.REACT_APP_API_URL;
    axios.interceptors.response.use(response => {
      return response
    }, err => {
      this.openErrorNotification(err.response.data.message);
      if (err.response.status === 401) {
        dispatch(logout());
      }
      return Promise.reject(err)
    });
  }

  openErrorNotification = (message) => {
    notification.error({
      message: message
    });
  };

  render() {
    const { history } = this.props;

    return (
      <ConnectedRouter history={history}>
        {routes}
      </ConnectedRouter>
    )
  }
}

App.propTypes = {
  history: PropTypes.object,
};

export default connect()(App)
