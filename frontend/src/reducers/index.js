import {combineReducers} from 'redux';
import { connectRouter } from 'connected-react-router';
import {pendingTasksReducer} from 'react-redux-spinner';
import menuInfo from './menuInfo';
import cardsInfo from './cardsInfo';
import cartInfo from './cartInfo';
import userInfo from './userInfo';
import checkoutInfo from './checkoutInfo';

export default (history) => combineReducers({
  router: connectRouter(history),
  pendingTasks: pendingTasksReducer,
  cardsInfo,
  menuInfo,
  cartInfo,
  userInfo,
  checkoutInfo
});