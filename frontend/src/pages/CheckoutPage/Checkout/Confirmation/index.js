import React from 'react';
import { Table } from 'antd';
import { Col, Row } from 'react-bootstrap';
import DeliveryInfo from './DeliveryInfo';
import Total from './Total';
import SmallButton from 'components/CustomComponents/SmallButton';
import { connect } from 'react-redux';

const mapStateToProps = ({ checkoutInfo }) => {
  return {
    orders: checkoutInfo.orders,
    productsPrice: checkoutInfo.productsPrice,
    shippingPrice: checkoutInfo.shippingPrice,
    totalPrice: checkoutInfo.totalPrice,
    shippingDescription: checkoutInfo.shippingDescription,
  }
};

class Confirmation extends React.Component {
  state = {
    dataSource: []
  };

  componentDidMount() {
    const { orders } = this.props;
    let tableData = [];
    orders.map(item => {
      tableData.push({
        item: item.productName,
        price: item.priceFormatted,
        options: item.attributes,
        key: item.id,
        quantity: item.amount,
      });
      return true
    });
    this.setState({
      dataSource: tableData
    })
  }


  render() {
    const { nextStep, totalPrice, shippingPrice, shippingDescription, productsPrice, deliveryData } = this.props,
      { dataSource } = this.state,

      columns = [{
        title: 'Item',
        dataIndex: 'item',
        key: 'item',
        align: 'center',
        width: '25%'
      }, {
        title: 'Options',
        dataIndex: 'options',
        key: 'options',
        align: 'center',
        width: '25%',
        render: (text, record) => {
          return record.options.map(option =>
            <p key={option}>
              {option}
            </p>
          )
        }
      }, {
        title: 'Qty',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        width: '25%',
      }, {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        width: '25%',
      }];

    return (
      <div>
        <div className="checkout-items-confirm">
          <Row>
            <Col xs={12} md={8}>
              <h3>Order summary</h3>
              <Table
                columns={columns}
                dataSource={dataSource}
                pagination={false}
              />
            </Col>
            <Col xs={12} md={4}>
              <DeliveryInfo
                shippingDescription={shippingDescription}
                deliveryData={deliveryData}
              />
            </Col>
            <Col>
              <Total
                productsPrice={productsPrice}
                shippingPrice={shippingPrice}
                totalPrice={totalPrice}
              />
            </Col>
          </Row>
        </div>
        <div className="checkout-buttons d-flex justify-content-center">
          <SmallButton
            text="Next"
            onClick={nextStep}
          />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Confirmation)