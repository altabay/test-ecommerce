<?php

namespace App\Form;

use App\DataObject\OrderDataObject;
use App\Entity\Shipping;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cartId', TextType::class, [
                'required' => true,
            ])
            ->add('firstName', TextareaType::class, [
                'required' => true,
            ])
            ->add('lastName', TextareaType::class, [
                'required' => true,
            ])
            ->add('country', TextareaType::class, [
                'required' => true,
            ])
            ->add('state', TextareaType::class, [
                'required' => true,
            ])
            ->add('zip', TextareaType::class, [
                'required' => true,
            ])
            ->add('city', TextareaType::class, [
                'required' => true,
            ])
            ->add('address', TextareaType::class, [
                'required' => true,
            ])
            ->add('shipping', EntityType::class, [
                'class' => Shipping::class,
                'required' => true,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderDataObject::class,
            'csrf_protection' => false,
        ]);
    }
}
