<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttributeValueRepository")
 */

class AttributeValue
{
    use IdTrait;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Attribute",
     *     inversedBy="attributeValues"
     * )
     */
    private $attribute;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $value;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->attribute . ':' . $this->value;
    }

    /**
     * @return Attribute
     */
    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    /**
     * @param Attribute $value
     *
     * @return $this
     */
    public function setAttribute(Attribute $value): self
    {
        $this->attribute = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
