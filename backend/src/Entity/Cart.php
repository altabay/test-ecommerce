<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    use IdTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32)
     */
    private $cartId;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Product"
     * )
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @var array [{'attributeId' => xxx, 'attributeName' => 'xxxxxxx', 'attributeValueId' => xxx, 'attributeValue' => 'xxxxxxx'}]
     *
     * @ORM\Column(type="json_array")
     */
    private $attributes;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    public function __construct()
    {
        $this->amount = 0;
        $this->attributes = [];
    }

    /**
     * @return string
     */
    public function getCartId(): string
    {
        return $this->cartId;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCartId(string $value): self
    {
        $this->cartId = $value;

        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $value
     *
     * @return $this
     */
    public function setProduct(Product $value): self
    {
        $this->product = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setAttributes(array $value): self
    {
        $this->attributes = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAmount(int $value): self
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $value
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $value): self
    {
        $this->createdAt = $value;

        return $this;
    }
}
