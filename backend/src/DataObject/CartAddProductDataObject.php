<?php

namespace App\DataObject;

use App\Entity\AttributeValue;
use App\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

class CartAddProductDataObject
{
    /**
     * @var string|null
     */
    private $cartId;

    /**
     * @var Product|null
     *
     * @Assert\NotNull()
     */
    private $product;

    /**
     * @var AttributeValue[]
     */
    private $attributeValues;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(value=1)
     */
    private $amount;

    public function __construct()
    {
        $this->attributeValues = [];
        $this->amount = 0;
    }

    /**
     * @return string|null
     */
    public function getCartId(): ?string
    {
        return $this->cartId;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setCartId(?string $value): self
    {
        $this->cartId = $value;

        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $value
     *
     * @return $this
     */
    public function setProduct(?Product $value): self
    {
        $this->product = $value;

        return $this;
    }

    /**
     * @return AttributeValue[]
     */
    public function getAttributeValues(): array
    {
        return $this->attributeValues;
    }

    /**
     * @param AttributeValue[]|null $value
     *
     * @return $this
     */
    public function setAttributeValues(?array $value): self
    {
        $this->attributeValues = (array)$value;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int|null $value
     *
     * @return $this
     */
    public function setAmount(?int $value): self
    {
        $this->amount = (int)$value;

        return $this;
    }
}
