const initialState = {
  step: 0,
  orders: [],
  regions: [],
  types: [],
  filterTypes: [],
  productsPrice: null,
  shippingPrice: null,
  totalPrice: null,
  total: null,
  shippingDescription: null,
  orderId: null,
  currency: null,
};

const checkoutInfo = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_STEP':
      return { ...state, step: action.step };
    case 'UPDATE_SHIPPING_DATA':
      return { ...state, ...action.params };
    case 'FILTER_TYPES_INFO':
      const newState = { ...state },
        { types } = newState,
        filterTypes = types.filter(item => item.regionId === Number(action.id));
      return { ...state, filterTypes };
    case 'UPDATE_ORDERS_DATA':
      return { ...state, orders: action.params };
    case 'UPDATE_ORDERS_PARAMS':
      return { ...state, ...action.param };
    default:
      return state
  }
};

export default checkoutInfo