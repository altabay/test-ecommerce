import React from 'react';
import './style.scss';
import CheckBox from "../CheckBox";
import Title from "../Title";

const Filter = (props) => {
  
  return (
    <div className="filter">
      <Title text={props.text}/>
     <CheckBox
       selected={props.selected}
       options={props.options}
       chooseFilter={props.chooseFilter}
       index={props.index}
     />
    </div>
  )
};

export default Filter