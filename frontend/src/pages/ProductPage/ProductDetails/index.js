import React from 'react';
import './style.scss';
import { Col, Container,  Row } from 'react-bootstrap';
import axios from 'axios';
import ImageDetails from './ImageDetails';
import Breadcrumbs from './Breadcrumbs';
import ProductDetailsForm from './ProductDetailsForm';

class ProductDetails extends React.Component {
  state = {
    imgActiveStatus: [],
    images: [],
    id: null,
    name: null,
    price: null,
    discountedPrice: null,
    description: null,
    attributes: [],
    selectedAttributes: {},
  };

  componentDidMount() {
    const { match } = this.props;
    axios.get(`products/get/${match.params.id}`)
      .then(resp => {
        const selectedAttributes = this.mapAttributes(resp.data.attributes);
        this.setState({
          name: resp.data.name,
          description: resp.data.description,
          price: resp.data.priceFormatted,
          discountedPrice: resp.data.discountedPrice,
          attributes: resp.data.attributes,
          selectedAttributes: selectedAttributes,
          id: resp.data.id,
          images: [resp.data.image, resp.data.image2]
        })
      })
  }

  mapAttributes = (attributes) => {
    let selectedAttributes = {};
    attributes.map(attribute => {
      selectedAttributes = { ...selectedAttributes, [attribute.name]: attribute.values[0].id };
      return true
    });

    return selectedAttributes
  };

  generateImgStatus = () => {
    let { imgActiveStatus, images } = this.state;
    images.forEach((img, index) => {
      imgActiveStatus[index] = 'not-active';
      if (index === 0) {
        imgActiveStatus[0] = 'active';
      }
    })
  };

  setActiveImg = imgNumber => {
    let { imgActiveStatus } = this.state;
    imgActiveStatus.forEach((imgStatus, index) => {
      imgActiveStatus[index] = 'not-active';
      if (imgNumber === index) {
        imgActiveStatus[index] = 'active'
      }
    });
    this.setState({
      imgActiveStatus: imgActiveStatus,
    })
  };

  chooseFilter = (name, id) => {
    const { selectedAttributes } = this.state,
      newSelectedAttributes = { ...selectedAttributes, [name]: Number(id) };

    this.setState({
      selectedAttributes: newSelectedAttributes
    })
  };

  componentWillMount() {
    this.generateImgStatus()
  }

  render() {
    const {
      imgActiveStatus,
      images,
      name,
      price,
      discountedPrice,
      description,
      attributes,
      selectedAttributes,
      id
    } = this.state;

    return (
      <Container fluid={true} className="my-3">
        <Row className="product-details">
          <Col lg={4}>
            <ImageDetails
              imgActiveStatus={imgActiveStatus}
              images={images}
              setActiveImg={this.setActiveImg}
              slider={this.slider}
            />
          </Col>
          <Col lg={8}>
            <Breadcrumbs title={name}/>
            <h4 className="product-details-main-title">
              <strong>{name}</strong>
            </h4>
            <div className="product-details-price">
              {discountedPrice
                ?
                <span>
                  {discountedPrice}
                  <div className="product-details-price-before">{price}</div>
                </span>
                : price
              }
            </div>
            <div className="product-details-descr mb-1">
              <p>{description}</p>
            </div>
            <ProductDetailsForm
              id={id}
              attributes={attributes}
              selectedAttributes={selectedAttributes}
              chooseFilter={this.chooseFilter}
            />
          </Col>
        </Row>
      </Container>

    )
  }
}

export default ProductDetails
