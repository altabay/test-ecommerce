<?php

namespace App\Pack;

use App\Entity\Order;
use App\Formatter\DateTimeFormatter;
use App\Formatter\PriceFormatter;

class OrderPack
{
    /**
     * @var OrderProductPack
     */
    private $orderProductPack;

    /**
     * @var PriceFormatter
     */
    private $priceFormatter;

    /**
     * @var DateTimeFormatter
     */
    private $dateTimeFormatter;

    /**
     * @var string
     */
    private $currency;

    /**
     * @param OrderProductPack $orderProductPack
     * @param PriceFormatter $priceFormatter
     * @param DateTimeFormatter $dateTimeFormatter
     * @param string $currency
     */
    public function __construct(
        OrderProductPack $orderProductPack,
        PriceFormatter $priceFormatter,
        DateTimeFormatter $dateTimeFormatter,
        string $currency
    ) {
        $this->orderProductPack = $orderProductPack;
        $this->priceFormatter = $priceFormatter;
        $this->dateTimeFormatter = $dateTimeFormatter;
        $this->currency = $currency;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function pack(Order $order): array
    {
        return [
            'id' => $order->getId(),
            'email' => $order->getUserEmail(),
            'firstName' => $order->getFirstName(),
            'lastName' => $order->getLastName(),
            'country' => $order->getCountry(),
            'state' => $order->getState(),
            'zip' => $order->getZip(),
            'city' => $order->getCity(),
            'address' => $order->getAddress(),
            'currency' => $this->currency,
            'productsPrice' => $order->getProductsPrice(),
            'productsPriceFormatted' => $this->priceFormatter->format($order->getProductsPrice()),
            'shippingPrice' => $order->getShippingPrice(),
            'shippingPriceFormatted' => $this->priceFormatter->format($order->getShippingPrice()),
            'totalPrice' => $order->getTotalPrice(),
            'totalPriceFormatted' => $this->priceFormatter->format($order->getTotalPrice()),
            'shippingDescription' => $order->getShippingDescription(),
            'createdAt' => $this->dateTimeFormatter->format($order->getCreatedAt()),
            'items' => $this->orderProductPack->packList($order->getProducts()->toArray()),
        ];
    }

    /**
     * @param array|Order[] $orders
     *
     * @return array
     */
    public function packList(array $orders): array
    {
        $list = [];

        foreach ($orders as $order) {
            $list[] = $this->pack($order);
        }

        return $list;
    }
}
