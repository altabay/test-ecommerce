<?php

namespace App\Form;

use App\DataObject\CartAddProductDataObject;
use App\Entity\AttributeValue;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CartAddProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('cartId', TextType::class)
            ->add('product', EntityType::class, [
                'class' => Product::class,
            ])
            ->add('attributeValues', EntityType::class, [
                'class' => AttributeValue::class,
                'multiple' => true,
            ])
            ->add('amount', IntegerType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CartAddProductDataObject::class,
            'method' => Request::METHOD_POST,
            'csrf_protection' => false,
        ]);
    }
}
