import React from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import SmallButton from 'components/CustomComponents/SmallButton';

const Finish = ({payment}) => {
  return (
    <div>
      {payment ?
        (
          <div className="checkout-items-finish text-center">
            <h1>Success!</h1>
            <Icon
              type="check-circle"
              className="checkout-items-finish-icon"
            />
          </div>
        ) : (
          <div className="checkout-items-finish text-center">
            <h1>Failed!</h1>
            <Icon
              type="warning"
              className="checkout-items-finish-icon"
            />
          </div>
        )
      }
      <div className="checkout-buttons d-flex justify-content-center">
        <Link to="/">
          <SmallButton
            text="Back to shop"
          />
        </Link>
      </div>
    </div>
  )
};

export default Finish