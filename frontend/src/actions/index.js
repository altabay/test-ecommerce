export const updatePaginationParams = (param) => {
  return {
    type: 'UPDATE_PAGINATION_PARAMS',
    param
  }
};

export const changeProductsInfo = (params) => {
  return {
    type: 'CHANGE_PRODUCTS_INFO',
    params
  }
};

export const changeCartId = (id) => {
  return {
    type: 'CHANGE_CART_ID',
    id
  }
};

export const changeCartData = (data) => {
  return {
    type: 'CHANGE_CART_DATA',
    data
  }
};

export const changeCartTotalPrice = (price) => {
  return {
    type: 'CHANGE_CART_TOTAL_PRICE',
    price
  }
};

export const changeStep = (step) => {
  return {
    type: 'CHANGE_STEP',
    step
  }
};

export const filterTypesInfo = (id) => {
  return {
    type: 'FILTER_TYPES_INFO',
    id
  }
};

export const updateShippingData = (params) => {
  return {
    type: 'UPDATE_SHIPPING_DATA',
    params
  }
};

export const updateOrdersData = (params) => {
  return {
    type: 'UPDATE_ORDERS_DATA',
    params
  }
};

export const updateOrdersParams = (param) => {
  return {
    type: 'UPDATE_ORDERS_PARAMS',
    param
  }
};

export const changeMenuInfo = (params) => {
  return {
    type: 'CHANGE_MENU_INFO',
    params
  }
};

export const filterCategoriesInfo = (id) => {
  return {
    type: 'FILTER_CATEGORIES_INFO',
    id
  }
};

export const updateMenuParams = (param) => {
  return {
    type: 'UPDATE_MENU_PARAMS',
    param
  }
};

export const registerOrLoginUser = (params) => {
  return {
    type: 'REGISTER_OR_LOGIN_USER',
    params
  }
};

export const logout = () => {
  return {
    type: 'LOGOUT'
  }
};