import React from 'react'
import { Route, Switch } from 'react-router';
import ProductPage from '../pages/ProductPage';
import CheckoutPage from '../pages/CheckoutPage';
import NotFoundPage from '../pages/NotFoundPage';
import ProductsCatalogPage from '../pages/ProductsCatalogPage';
import Helmet from 'react-helmet';
import MenuTop from '../components/CustomComponents/MenuTop';
import ScrollUpButton from 'react-scroll-up-button';
import { Redirect } from 'react-router';

const routes = (
  <div>
    <Helmet titleTemplate="TShirtShop - %s"/>
    <MenuTop/>
    <Switch>
      <Route exact path="/page/:page_no" component={match => <ProductsCatalogPage config={match}/>}/>
      <Redirect exact from="/" to="/page/1"/>
      <Route path="/product/:id" component={ProductPage}/>
      <Route path="/checkout" component={CheckoutPage}/>
      <Route component={NotFoundPage}/>
    </Switch>
    <ScrollUpButton/>
  </div>
);

export default routes