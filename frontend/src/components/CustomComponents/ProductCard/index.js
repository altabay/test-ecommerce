import React from 'react';
import './style.scss';
import ProductCardInformation from './ProductCardInformation';

const ProductCard = ({ product }) => {

  return (
    <div className="product-card">
      <div className="product-card-img">
        <img src={product.image} alt=""/>
      </div>
      <div className="product-card-title">
        <p>{product.name}</p>
        <div className="product-card-price">
          {product.discountedPrice
            ? <span>
             <span>
              {product.discountedPriceFormatted}
            </span>
            <div className="product-card-price-old">
              {product.priceFormatted}
              </div>
            </span>
            : <span>
              {product.priceFormatted}
            </span>
          }
        </div>
      </div>
      <ProductCardInformation product={product}/>
    </div>
  )
};

export default ProductCard
