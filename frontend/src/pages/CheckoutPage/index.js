import React from 'react';
import Helmet from 'react-helmet';
import Checkout from './Checkout';

class CheckoutPage extends React.Component {

  render() {
    return (
      <div>
        <Helmet title="Checkout" />
        <Checkout />
      </div>
    )
  }
}

export default CheckoutPage