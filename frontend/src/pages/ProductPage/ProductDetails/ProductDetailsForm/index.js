import React from 'react';
import Button from 'components/CustomComponents/Button';
import Filter from 'components/CustomComponents/Filter';
import { Col, Form, Row } from 'react-bootstrap';
import axios from 'axios';
import Quantity from 'components/CustomComponents/Quantity';
import Title from 'components/CustomComponents/Title';
import { connect } from 'react-redux';
import { changeCartId, changeCartData, changeCartTotalPrice } from 'actions';

const mapStateToProps = ({ cartInfo }) => {
  return {
    cartId: cartInfo.cartId,
  }
};

class ProductDetails extends React.Component {
  state = {
    amount: 1
  };

  changeAmount = (value) => {
    this.setState({
      amount: value
    })
  };

  attributeArrayPicker = () => {
    const { selectedAttributes } = this.props;
    let attributes = [];
    for (let key in selectedAttributes) {
      attributes.push(selectedAttributes[key])
    }
    return attributes
  };

  addProductToCart = (e) => {
    const { cartId, id, dispatch } = this.props;
    const { amount } = this.state;
    e.preventDefault();
    axios.post('cart/add',
      {
        cartId,
        product: id,
        attributeValues: this.attributeArrayPicker(),
        amount
      }
    )
      .then(resp => {
        dispatch(changeCartId(resp.data.cartId));
        dispatch(changeCartData(resp.data.items));
        dispatch(changeCartTotalPrice(resp.data.totalFormatted));
      })
      .catch(err => {
        console.log(err)
      })
  };

  render() {
    const { attributes, selectedAttributes, chooseFilter } = this.props;
    const { amount } = this.state;

    return (
      <Form onSubmit={this.addProductToCart}>
        <div className='product-details-quantity'>
          <Title text='Quantity'/>
          <Quantity
            amount={amount}
            changeAmount={this.changeAmount}
          />
        </div>
        <Row>
          <Col>
            {attributes.map(attribute => {
                return (
                  <Filter
                    key={attribute.id}
                    selected={selectedAttributes[attribute.name]}
                    chooseFilter={chooseFilter}
                    text={attribute.name}
                    options={attribute.values}
                    index={attribute.name}
                  />
                )
              }
            )}
          </Col>
        </Row>
        <div className='product-details-controls'>
          <Button
            text='Add to cart'
            type='submit'
          />
        </div>
      </Form>

    )
  }
}

export default connect(mapStateToProps)(ProductDetails)
