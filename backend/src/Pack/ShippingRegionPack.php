<?php

namespace App\Pack;

use App\Entity\ShippingRegion;

class ShippingRegionPack
{
    /**
     * @var ShippingPack
     */
    private $shippingPack;

    /**
     * @param ShippingPack $shippingPack
     */
    public function __construct(ShippingPack $shippingPack)
    {
        $this->shippingPack = $shippingPack;
    }

    /**
     * @param ShippingRegion $shippingRegion
     *
     * @return array
     */
    public function pack(ShippingRegion $shippingRegion): array
    {
        return [
            'id' => $shippingRegion->getId(),
            'name' => (string)$shippingRegion->getName(),
            'shippings' => $this->shippingPack->packList($shippingRegion->getShippings()->toArray()),
        ];
    }

    /**
     * @param array|ShippingRegion[] $shippingRegions
     *
     * @return array
     */
    public function packList(array $shippingRegions): array
    {
        $list = [];

        foreach ($shippingRegions as $shippingRegion) {
            $list[] = $this->pack($shippingRegion);
        }

        return $list;
    }
}
