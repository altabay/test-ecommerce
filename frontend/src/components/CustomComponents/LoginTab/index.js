import React from 'react';
import { Form, Col } from 'react-bootstrap';
import SocialLogin from '../SocialLogin';
import SmallButton from '../SmallButton';
import axios from 'axios';
import { registerOrLoginUser, changeStep } from 'actions';
import { connect } from 'react-redux';

class LoginTab extends React.Component {
  state = {
    email: '',
    password: '',
    is_remember: true,
    validated: false,
  };

  changeData = (name, e) => {
    this.setState({
      [name]: e.target.value
    })
  };

  validate = (e) => {
    const form = e.currentTarget;
    e.preventDefault();
    e.stopPropagation();
    if (form.checkValidity()) {
      this.login();
    } else {
      this.setState({ validated: true });
    }
  };

  login = () => {
    const { dispatch, success } = this.props;
    const { email, password } = this.state;
    axios.post('user/login', {
      email,
      password
    })
      .then(resp => {
        dispatch(registerOrLoginUser({
          token: resp.data.token,
          email: resp.data.email
        }));
        dispatch(changeStep(1));
        success();
      })
  };

  render() {
    const { validated } = this.state;
    const { success } = this.props;

    return (
      <div className="my-3">
        <Form
          onSubmit={this.validate}
          noValidate
          validated={validated}
        >
          <Form.Row className="justify-content-center">
            <Form.Group as={Col} xs={12}>
              <Form.Control
                required
                type="email"
                placeholder="Email"
                onChange={e => this.changeData('email', e)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid email.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} xs={12}>
              <Form.Control
                required
                type="password"
                placeholder="Password"
                onChange={e => this.changeData('password', e)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} xs={5} className="text-center">
              <SmallButton
                text="Login"
                type="submit"
              />
            </Form.Group>
          </Form.Row>
        </Form>
        <SocialLogin
          hideModal={success}
          type="Log In"
        />
      </div>
    )
  }
}

export default connect()(LoginTab);
