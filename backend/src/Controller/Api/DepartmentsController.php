<?php

namespace App\Controller\Api;

use App\Entity\Department;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(
 *     path="/api/departments"
 * )
 */
class DepartmentsController extends FOSRestController
{
    /**
     * @Rest\Route(
     *     path="/all",
     *     methods={"GET"},
     *     defaults={
     *          "_format": "json"
     *     }
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Request $request
     *
     * @return View
     */
    public function allAction(ParamFetcherInterface $paramFetcher, Request $request)
    {
        $list = $this
            ->getDoctrine()
            ->getRepository(Department::class)
            ->findAll()
        ;

        return $this->view(
            $this->get('app.pack.department_pack')->packList($list),
            Response::HTTP_OK
        );
    }
}
