import React from "react";
import { GoogleLogin } from 'react-google-login';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import './style.scss';
import axios from 'axios';
import { registerOrLoginUser } from 'actions';
import { connect } from 'react-redux';

class SocialLogin extends React.Component {

  responseGoogle = (response) => {
    const { dispatch, hideModal } = this.props;
    axios.post(`user/google/${response.accessToken}`)
      .then(resp => {
        dispatch(registerOrLoginUser({
          token: resp.data.token,
          email: resp.data.email
        }));
        if (hideModal) {
          hideModal();
        }
      })
  };

  render() {
    const { type } = this.props;
    const icon = (<FontAwesomeIcon icon={faGoogle}/>);

    return (
      <div>
        <h5 className="another-service-label">Use another service to {type}</h5>
        <GoogleLogin
          clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
          buttonText={icon}
          render={renderProps => (
            <button
              className="another-service-button btn btn-danger"
              onClick={renderProps.onClick}>
              {icon}
            </button>
          )}
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          icon={false}
        />
      </div>
    )
  }
}

export default connect()(SocialLogin)