import React from 'react';
import Button from '../../../Button';
import Select from '../../../Select';
import { Form } from 'antd';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { connect } from 'react-redux';
import { changeCartId, changeCartData, changeCartTotalPrice } from 'actions';

const mapStateToProps = ({ cartInfo }) => {
  return {
    cartId: cartInfo.cartId,
  }
};

class ProductCardForm extends React.Component {
  state = {
    attributesValues: {}
  };

  componentDidMount() {
    const { attributes } = this.props;
    if (attributes) {
      const newAttributesValues = this.mapAttributes(attributes);
      this.setState({
        attributesValues: newAttributesValues
      })
    }
  }

  changeAttributesValue = (id, name) => {
    const { attributesValues } = this.state,
      newAttributesValues = { ...attributesValues, [name]: Number(id) };

    this.setState({
      attributesValues: newAttributesValues
    })
  };

  mapAttributes = (attributes) => {
    let selectedAttributes = {};
    attributes.map(attribute => {
      selectedAttributes = { ...selectedAttributes, [attribute.name]: attribute.values[0].id };
      return true
    });

    return selectedAttributes
  };

  attributeArrayPicker = () => {
    const { attributesValues } = this.state;
    let attributes = [];
    for (let key in attributesValues) {
      attributes.push(attributesValues[key])
    }
    return attributes
  };

  addProductToCart = (e) => {
    const { cartId, id, dispatch } = this.props;
    e.preventDefault();
    axios.post('cart/add',
      {
        cartId,
        product: id,
        attributeValues: this.attributeArrayPicker(),
        amount: 1
      }
    )
      .then(resp => {
        dispatch(changeCartId(resp.data.cartId));
        dispatch(changeCartData(resp.data.items));
        dispatch(changeCartTotalPrice(resp.data.totalFormatted));
      })
  };

  render() {
    const { attributes } = this.props;
    const { attributesValues } = this.state;

    return (
      <Form
        className="product-card-form"
        onSubmit={this.addProductToCart}
      >
        <div className="product-card-form-select">
          <Row className="justify-content-center">
            {attributes && attributes.map(attribute =>
              <Col xs={4} key={attribute.id}>
                <Select
                  value={attributesValues[attribute.name]}
                  name={attribute.name}
                  options={attribute.values}
                  changeAttributesValue={this.changeAttributesValue}
                />
              </Col>
            )}
          </Row>
        </div>
        <Button
          type="submit"
          className="product-card-info-button"
          text="Add to cart"
        />
      </Form>
    )
  }
}


export default connect(mapStateToProps)(ProductCardForm)
