import React from 'react';
import Title from 'components/CustomComponents/Title';

const DeliveryInfo = ({ deliveryData, shippingDescription }) => {

  return (
    <div className="checkout-items-confirm-info">
      <h3>Delivery</h3>
      <Title text="Address"/>
      <p>{deliveryData.firstName} {deliveryData.lastName},</p>
      <p>{deliveryData.address},</p>
      <p>{deliveryData.city}, {deliveryData.state}, {deliveryData.country}, {deliveryData.zip}</p>
      <Title text="Delivery options"/>
      <p>{shippingDescription}</p>
    </div>
  )
};

export default DeliveryInfo