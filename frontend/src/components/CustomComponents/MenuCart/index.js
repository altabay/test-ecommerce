import React from 'react';
import './style.scss';
import { Row, Col } from 'react-bootstrap';
import SmallButton from '../SmallButton';
import Title from '../Title';
import CartModal from '../CartModal';
import { connect } from 'react-redux';
import axios from 'axios';
import { changeCartData, changeCartTotalPrice } from 'actions';
import MenuCartItem from './MenuCartItem';

const mapStateToProps = ({ cartInfo }) => {
  return {
    cartId: cartInfo.cartId,
    totalPrice: cartInfo.totalPrice,
    cartData: cartInfo.cartData,
  }
};

class MenuCart extends React.Component {
  state = {
    show: false,
  };

  componentDidMount() {
    const { cartId, dispatch } = this.props;
    if (cartId) {
      axios.get(`cart/get/${cartId}`)
        .then(resp => {
          dispatch(changeCartData(resp.data.items));
          dispatch(changeCartTotalPrice(resp.data.totalFormatted));
        })
    }
  }

  toggleCartModal = () => {
    const { show } = this.state;
    this.setState({
      show: !show
    })
  };

  render() {
    const { show } = this.state,
      { totalPrice, cartData } = this.props;

    return (
      <div className="cart">
        {Boolean(cartData.length) && (
          <div>
            <Title text="Cart"/>
            {cartData.map(cartItem =>
              <MenuCartItem
                key={cartItem.cartProductId}
                cartItem={cartItem}
              />
            )}
            <div className="text-right total-price">
              Total Price:
              <span className="total-price-value">{totalPrice}</span>
            </div>
            <Row>
              <Col className="d-flex justify-content-center">
                <SmallButton
                  type="button"
                  text="View Details"
                  onClick={this.toggleCartModal}
                />
              </Col>
            </Row>
          </div>
        )}
        <CartModal
          toggleCartModal={this.toggleCartModal}
          show={show}
          showButton={Boolean(cartData.length)}
        />
      </div>
    )
  }
}

export default connect(mapStateToProps)(MenuCart)