<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderProductRepository")
 */
class OrderProduct
{
    use IdTrait;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Order",
     *     inversedBy="products"
     * )
     */
    private $order;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Product"
     * )
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $productName;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $productPrice;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $attributes;

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $value
     *
     * @return $this
     */
    public function setOrder(Order $value): self
    {
        $this->order = $value;

        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $value
     *
     * @return $this
     */
    public function setProduct(?Product $value): self
    {
        $this->product = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setProductName(string $value): self
    {
        $this->productName = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAmount(int $value): self
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getProductPrice(): float
    {
        return $this->productPrice;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setProductPrice(float $value): self
    {
        $this->productPrice = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setAttributes(array $value): self
    {
        $this->attributes = $value;

        return $this;
    }
}
