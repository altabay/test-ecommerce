<?php

namespace App\Cart;

use App\Entity\AttributeValue;
use App\Entity\Cart;
use App\Entity\Product;
use App\Product\ProductAttributesValidate;
use App\Repository\CartRepository;

class CartProductAdder
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var ProductAttributesValidate
     */
    private $productAttributesValidate;

    /**
     * @var CartAttributesConverter
     */
    private $cartAttributesConverter;

    /**
     * @param CartRepository $cartRepository
     * @param ProductAttributesValidate $productAttributesValidate
     * @param CartAttributesConverter $cartAttributesConverter
     */
    public function __construct(
        CartRepository $cartRepository,
        ProductAttributesValidate $productAttributesValidate,
        CartAttributesConverter $cartAttributesConverter
    ) {
        $this->cartRepository = $cartRepository;
        $this->productAttributesValidate = $productAttributesValidate;
        $this->cartAttributesConverter = $cartAttributesConverter;
    }

    /**
     * @param string|null $cartId
     * @param Product $product
     * @param int $amount
     * @param array|AttributeValue[] $attributes
     *
     * @return Cart
     * @throws \Exception
     */
    public function add(?string $cartId, Product $product, int $amount, array $attributes = []): Cart
    {
        $this->productAttributesValidate->validateAttributes($product, $attributes);
        $cart = $cartId
            ? $this->checkGenerateCart($cartId, $product, $attributes)
            : $this->generateCart($product, $this->generateCartId(), $attributes)
        ;

        return $cart->setAmount($cart->getAmount() + $amount);
    }

    /**
     * @param string $cartId
     * @param Product $product
     * @param array|AttributeValue[] $attributes
     *
     * @return Cart
     * @throws \Exception
     */
    private function checkGenerateCart(string $cartId, Product $product, array $attributes): Cart
    {
        $carts = $this->cartRepository->findBy([
            'cartId' => $cartId,
            'product' => $product->getId(),
        ]);

        if (!count($carts)) {
            return $this->generateCart($product, $cartId, $attributes);
        }

        $searchAttributes = $this->cartAttributesConverter->arraySortToString(
            $this->cartAttributesConverter->arrayValuesToArray($attributes)
        );

        /** @var Cart $cart */
        foreach ($carts as $cart) {
            $cartAttributes = $this->cartAttributesConverter->arraySortToString($cart->getAttributes());

            if ($cartAttributes === $searchAttributes) {
                return $cart;
            }
        }

        return $this->generateCart($product, $cartId, $attributes);
    }

    /**
     * @param Product $product
     * @param array|AttributeValue[] $attributes
     *
     * @return Cart
     * @throws \Exception
     */
    private function generateCart(Product $product, $cartId, array $attributes): Cart
    {
        $cart = (new Cart())
            ->setCartId($cartId)
            ->setProduct($product)
            ->setAttributes($this->cartAttributesConverter->arrayValuesToArray($attributes))
        ;

        return $cart;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function generateCartId(): string
    {
        $salt = random_int(10000, 99999);

        return md5($salt . '|' . time());
    }
}
