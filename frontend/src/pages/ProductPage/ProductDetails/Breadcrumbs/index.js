import React from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const mapStateToProps = ({ cardsInfo }) => {
  return {
    page: cardsInfo.page,
  }
};

const Breadcrumbs = ({ title, page }) => {

  return (
    <Breadcrumb>
      <Breadcrumb.Item>
        <Link to={"/page/" + page}>
          Home
        </Link>
      </Breadcrumb.Item>
      <Breadcrumb.Item>{title}</Breadcrumb.Item>
    </Breadcrumb>
  )
};

export default connect(mapStateToProps)(Breadcrumbs)