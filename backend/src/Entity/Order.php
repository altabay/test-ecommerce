<?php

namespace App\Entity;

use App\Entity\Traits\CreatedUpdatedTrait;
use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{
    public const STATUS_NEW = 'new';
    public const STATUS_PAID = 'paid';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_FAILED = 'failed';

    use IdTrait;
    use CreatedUpdatedTrait;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\User"
     * )
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $productsPrice;

    /**
     * @var float shipping inclusive
     *
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $totalPrice;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $shippingDescription;

    /**
     * @var Shipping|null
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Shipping"
     * )
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     *
     */
    private $shipping;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $status;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $paymentResponse;

    /**
     * @var OrderProduct[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\OrderProduct",
     *     mappedBy="product",
     *     cascade={"persist","remove"}
     * )
     */
    private $products;

    public function __construct()
    {
        $this->status = self::STATUS_NEW;
        $this->productsPrice = 0;
        $this->totalPrice = 0;
        $this->products = new ArrayCollection();
        $this->paymentResponse = [];
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $value
     *
     * @return $this
     */
    public function setUser(?User $value): self
    {
        $this->user = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setUserEmail(string $value): self
    {
        $this->userEmail = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setFirstName(string $value): self
    {
        $this->firstName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setLastName(string $value): self
    {
        $this->lastName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCountry(string $value): self
    {
        $this->country = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setState(string $value): self
    {
        $this->state = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setZip(string $value): self
    {
        $this->zip = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCity(string $value): self
    {
        $this->city = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setAddress(string $value): self
    {
        $this->address = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getProductsPrice(): float
    {
        return $this->productsPrice;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setProductsPrice(float $value): self
    {
        $this->productsPrice = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setTotalPrice(float $value): self
    {
        $this->totalPrice = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getShippingPrice(): float
    {
        return $this->getTotalPrice() - $this->getProductsPrice();
    }

    /**
     * @return string
     */
    public function getShippingDescription(): string
    {
        return $this->shippingDescription;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setShippingDescription(string $value): self
    {
        $this->shippingDescription = $value;

        return $this;
    }

    /**
     * @return Shipping|null
     */
    public function getShipping(): ?Shipping
    {
        return $this->shipping;
    }

    /**
     * @param Shipping|null $value
     *
     * @return $this
     */
    public function setShipping(?Shipping $value): self
    {
        $this->shipping = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setStatus(string $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @param OrderProduct $value
     *
     * @return $this
     */
    public function addProduct(OrderProduct  $value): self
    {
        if (!$this->products->contains($value)) {
            $this->products->add($value);
        }

        return $this;
    }

    /**
     * @param OrderProduct $value
     *
     * @return $this
     */
    public function removeProduct(OrderProduct $value): self
    {
        if ($this->products->contains($value)) {
            $this->products->removeElement($value);
        }

        return $this;
    }

    /**
     * @return OrderProduct[]|Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return array
     */
    public function getPaymentResponse(): array
    {
        return $this->paymentResponse;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setPaymentResponse(array $value): self
    {
        $this->paymentResponse = $value;

        return $this;
    }
}
