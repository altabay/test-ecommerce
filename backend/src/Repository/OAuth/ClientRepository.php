<?php

namespace App\Repository\OAuth;

use App\Entity\OAuth\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ClientRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @return Client|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findFirst(): ?Client
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->setMaxResults(1)
            ->orderBy('c.id')
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
