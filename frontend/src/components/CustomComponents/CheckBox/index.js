import React from 'react';
import './style.scss';
import { Col, Row } from "react-bootstrap";

const CheckBox = (props) => {
  return (
    <Row className="check-box">
      {props.options.map(item => {
          let classes = props.selected === item.id
            ? "check-box-option check-box-option-selected"
            : "check-box-option";
          return (
            <Col  key={item.id}>
              <h3
                className={classes}
                onClick={() => props.chooseFilter(props.index, item.id)}
              >
                {item.name}
              </h3>
            </Col>
          )
        }
      )}
    </Row>
  )
};

export default CheckBox