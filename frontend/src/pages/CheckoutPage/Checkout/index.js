import React from 'react';
import Delivery from './Delivery';
import Confirmation from './Confirmation';
import Payment from './Payment';
import Finish from './Finish';
import './style.scss';
import { Container } from 'react-bootstrap';
import Progress from './Progress';
import { connect } from 'react-redux';
import LoginOrSignUp from './LoginOrSignUp';
import { changeStep } from 'actions';

const mapStateToProps = ({ userInfo, checkoutInfo }) => {
  return {
    token: userInfo.token,
    step: checkoutInfo.step,
  }
};

class Wizard extends React.Component {
  state = {
    payment: false,
    deliveryData: {
      firstName: '',
      lastName: '',
      address: '',
      city: '',
      state: '',
      zip: '',
      region: '',
      shipping: '',
      country: '',
    }
  };

  componentDidMount() {
    const { token } = this.props;
    if (token) {
      this.authUser();
    }
  }

  authUser = () => {
    const { dispatch } = this.props;
    dispatch(changeStep(1));
  };

  changeDeliveryData = (key, value) => {
    const { deliveryData } = this.state;
    this.setState({
      deliveryData: {
        ...deliveryData,
        [key]: value
      }
    })
  };

  payment = () => {
    this.setState({
      payment: true
    })
  };

  nextStep = () => {
    const { step, dispatch } = this.props;
    dispatch(changeStep(step + 1));
  };

  prevStep = () => {
    const { step, dispatch } = this.props;
    dispatch(changeStep(step - 1));
  };

  renderSwitch(step) {
    const { deliveryData, payment } = this.state;
    switch (step) {
      case 0:
        return (
          <LoginOrSignUp
            nextStep={this.authUser}
          />
        );
      case 1:
        return (
          <Delivery
            nextStep={this.nextStep}
            changeDeliveryData={this.changeDeliveryData}
            deliveryData={deliveryData}
          />
        );
      case 2:
        return (
          <Confirmation
            nextStep={this.nextStep}
            deliveryData={deliveryData}
          />
        );
      case 3:
        return (
          <Payment
            payment={this.payment}
            nextStep={this.nextStep}
          />
        );
      case 4:
        return (
          <Finish
            payment={payment}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
          />
        );
      default:
        return null
    }
  };

  render() {
    const { step } = this.props;

    return (
      <Container fluid={true} className="my-3">
        <div className="checkout">
          <div className="checkout-items">
            <Progress step={step}/>
            {this.renderSwitch(step)}
          </div>
        </div>
      </Container>
    )

  }
}

export default connect(mapStateToProps)(Wizard)
