import React from 'react';
import './style.scss';

const Title = (props) => {

  return (
    <h3 className={"title " + props.className}>
      {props.text}
    </h3>
  )
};

export default Title