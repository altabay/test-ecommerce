<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 07.03.19
 * Time: 12:29
 */

namespace App\Pack;

use App\Entity\AttributeValue;

class AttributeValuePack
{
    /**
     * @param AttributeValue $attributeValue
     *
     * @return array
     */
    public function pack(AttributeValue $attributeValue): array
    {
        return [
            'id' => $attributeValue->getId(),
            'name' => (string)$attributeValue->getValue(),
        ];
    }

    /**
     * @param array|AttributeValue[] $attributeValues
     *
     * @return array
     */
    public function packList(array $attributeValues): array
    {
        $list = [];

        foreach ($attributeValues as $attributeValue) {
            $list[] = $this->pack($attributeValue);
        }

        return $list;
    }
}