Turing
======

## Install

Install packages:
* nginx
* mysql 5.7
* php7.2-fpm
* composer
* npm
* yarn

### Frontend settings
#### Setting up OAuth 2.0 for Google authorization:
   * Go to the [API Console](https://console.developers.google.com).
   * From the projects list, select a project or create a new one.
   * If the APIs & services page isn't already open, open the console left side menu and select APIs & services.
   * On the left, click Credentials.
   * Click New Credentials, then select OAuth client ID.
      * Note: If you're unsure whether OAuth 2.0 is appropriate for your project, select Help me choose and follow the instructions to pick the right credentials.
     
   * Select the appropriate application type for your project and enter any additional information required. Application types are described in more detail in the following sections.
   * If this is your first time creating a client ID, you can also configure your consent screen by clicking Consent Screen. (The following procedure explains how to set up the Consent screen.) You won't be prompted to configure the consent screen after you do it the first time.
   * Click Create client ID
     
####PayPal config: 
   * From the [My Apps & Credentials page](https://developer.paypal.com/developer/applications/), log in with your PayPal account.
   * Under REST API apps, click Create App.
   * Enter an App Name and click Create App. PayPal displays information about your app. You can review and modify the app details.
      * Note: In the top right-hand corner, you can toggle between sandbox and live app information.
     
   * Click Save.
   * Go live. Before you go live, you might need to upgrade your PayPal account.
   

###Run scripts:
* From backend folder:
  * `cp .env .env.local` + edit DB connection string
  * `bin/install`
  * `bin/console fos:user:create admin` here you'll be asked for values: email (any email), passord (to be used later for access to admin)
  * `bin/console fos:user:promote admin ROLE_ADMIN` this will promote your user to admin, othervise you'll not be able to access /admin/

* From frontend folder:
  * `cp .env.dist .env` + edit env variables
  * `bin/update` 

### NGINX configs
#### Backend
```
server {
    server_name backend.turing.loc;
    root /[project_folder]/backend/public;

    location / {
     if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Authorization';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
     }
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/backend.turing_error.log;
    access_log /var/log/nginx/backend.turing_access.log;
}
``` 
#### Frontend
```
server {
    listen 80;
    server_name turing.loc;
    root /home/katya/Projects/turing-ecommerce/frontend/build;

    index index.html;

    location / {
        try_files $uri @prerender;
    }

    location @prerender {
        set $prerender 0;
        if ($http_user_agent ~* "googlebot|bingbot|yandex|baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator") {
            set $prerender 1;
        }
        if ($args ~ "_escaped_fragment_") {
            set $prerender 1;
        }
        if ($http_user_agent ~ "Prerender") {
            set $prerender 0;
        }
        if ($uri ~* "\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff|svg|eot)") {
            set $prerender 0;
        }

        resolver 8.8.8.8;

        if ($prerender = 1) {

            set $prerender "service.prerender.io";
            rewrite .* /$scheme://$host$request_uri? break;
            proxy_pass http://$prerender;
        }
        if ($prerender = 0) {
            rewrite .* /index.html break;
        }
    }

    error_log /var/log/nginx/frontend.turing_error.log;
    access_log /var/log/nginx/frontend.turing_access.log;
}
```

#Usage details
* You need to be logged in to PayPal while passing through sandbox payment process
