<?php

namespace App\DataObject;

use App\Entity\Cart;
use Symfony\Component\Validator\Constraints as Assert;

class CartProductDataObject
{
    /**
     * @var Cart
     *
     * @Assert\NotNull()
     */
    private $cart;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $cartId;

    public function __construct()
    {
        $this->cartId = '';
    }

    /**
     * @return Cart|null
     */
    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    /**
     * @param Cart|null $value
     *
     * @return $this
     */
    public function setCart(?Cart $value): self
    {
        $this->cart = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartId(): string
    {
        return $this->cartId;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setCartId(string $value): self
    {
        $this->cartId = $value;

        return $this;
    }
}
