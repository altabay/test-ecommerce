<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $value
     *
     * @return User|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUserByUsernameOrEmail(string $value): ?User
    {
        $value = strtolower($value);
        $qb = $this->createQueryBuilder('u');
        $qb
            ->where('u.username = :value OR u.email = :value')
            ->andWhere('u.enabled = :enabled')
            ->setParameter(':value', $value)
            ->setParameter('enabled', 1)
            ->orderBy('u.id')
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }


}
