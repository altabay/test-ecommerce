<?php

namespace App\Order;

use App\Entity\Cart;
use App\Entity\OrderProduct;

class OrderProductConverter
{
    /**
     * @param Cart $cart
     *
     * @return OrderProduct
     */
    public function cartToOrderProduct(Cart $cart): OrderProduct
    {
        return (new OrderProduct())
            ->setProduct($cart->getProduct())
            ->setProductName($cart->getProduct()->getName())
            ->setAmount($cart->getAmount())
            ->setProductPrice($cart->getProduct()->getFinalPrice())
            ->setAttributes($this->prepareAttributes($cart))
        ;
    }

    /**
     * @param Cart $cart
     *
     * @return array
     */
    private function prepareAttributes(Cart $cart): array
    {
        $list = [];

        foreach ($cart->getAttributes() as $attribute) {
            $list[] = sprintf('%s:%s', $attribute['attributeName'], $attribute['attributeValue']);
        }

        sort($list);

        return $list;
    }
}
