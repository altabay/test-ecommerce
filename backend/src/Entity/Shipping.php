<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShippingRepository")
 */

class Shipping
{
    use IdTrait;

    /**
     * @var ShippingRegion
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\ShippingRegion",
     *     inversedBy="shippings"
     * )
     */
    private $shippingRegion;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $shippingType;

    /**
     * @var float
     *
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $shippingCost;

    /**
     * @return ShippingRegion
     */
    public function getShippingRegion(): ?ShippingRegion
    {
        return $this->shippingRegion;
    }

    /**
     * @param ShippingRegion $value
     *
     * @return $this
     */
    public function setShippingRegion(ShippingRegion $value): self
    {
        $this->shippingRegion = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getShippingType(): ?string
    {
        return $this->shippingType;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setShippingType(string $value): self
    {
        $this->shippingType = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getShippingCost(): ?float
    {
        return $this->shippingCost;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setShippingCost(float $value): self
    {
        $this->shippingCost = $value;

        return $this;
    }
}
