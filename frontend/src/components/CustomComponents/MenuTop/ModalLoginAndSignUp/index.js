import React from 'react';
import { Modal, Tabs, Tab } from 'react-bootstrap';
import LoginTab from '../../LoginTab';
import SignUpTab from '../../SignUpTab';

class ModalLoginAndSignUp extends React.Component {
  state = {
    key: 'login'
  };

  changeTab = (key) => {
    this.setState({ key })
  };

  render() {
    const { show, toggleModal } = this.props,
      { key } = this.state;
    return (
      <Modal
        show={show}
        onHide={toggleModal}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton/>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={key => this.changeTab(key)}
          >
            <Tab eventKey="login" title="Login">
              <LoginTab
                success={toggleModal}
              />
            </Tab>
            <Tab eventKey="signUp" title="Sign Up">
              <SignUpTab
                success={toggleModal}
              />
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    )
  }
}

export default ModalLoginAndSignUp;