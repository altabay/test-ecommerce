<?php

namespace App\Order;

use App\DataObject\PaymentDataObject;
use App\Entity\Order;

class PaymentAdder
{
    /**
     * @var array
     */
    private $transition = [
        'success' => Order::STATUS_PAID,
        'failed' => Order::STATUS_FAILED,
        'canceled' => Order::STATUS_CANCELED,
    ];

    /**
     * @param Order $order
     * @param PaymentDataObject $paymentDataObject
     *
     * @throws \Exception
     */
    public function add(Order $order, PaymentDataObject $paymentDataObject): void
    {
        if (!isset($this->transition[$paymentDataObject->getStatus()])) {
            throw new \Exception(sprintf('Unknown status "%s".', $paymentDataObject->getStatus()), 400);
        }

        $order
            ->setStatus($this->transition[$paymentDataObject->getStatus()])
            ->setPaymentResponse(json_decode($paymentDataObject->getResponseData(), true))
        ;
    }
}
